<?php

namespace PLU\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Agenda
 *
 * @ORM\Table(name="plu_agenda")
 * @ORM\Entity(repositoryClass="PLU\CoreBundle\Repository\AgendaRepository")
 */
class Agenda
{

    /**
    * @ORM\ManyToMany(targetEntity="PLU\CoreBundle\Entity\Licence", inversedBy="agendas")
    * @ORM\JoinTable(
        *  name="plu_agenda_licences",
        *  joinColumns={
        *      @ORM\JoinColumn(name="agenda_id", referencedColumnName="id")
        *  },
        *  inverseJoinColumns={
        *      @ORM\JoinColumn(name="licence_id", referencedColumnName="id")
        *  }
        * )
    */
    private $licences;

    /**
     * @ORM\ManyToOne(targetEntity="PLU\CoreBundle\Entity\Utilisateur")
     * @ORM\JoinColumn(name="utilisateur_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $utilisateur;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     * @Assert\DateTime()
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     * @Assert\GreaterThan("yesterday", message="La date ne peut pas être inférieure à la date actuelle.")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     * @Assert\Length(
     *      max=255,
     *      min=10,
     *      maxMessage="La description est limité à {{ limit }} caractères maximum.",
     *      minMessage="La description est limité à {{ limit }} caractères minimum.",
     * )
     */
    private $description;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Agenda
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Agenda
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function getLicencesArray() {
        foreach ($this->getLicences() as $key => $value) {
            $licencesArray[$value->getId()] = $value->getId();
        }
        return $licencesArray;
    }

    /**
     * Set utilisateur
     *
     * @param \PLU\CoreBundle\Entity\Utilisateur $utilisateur
     *
     * @return Agenda
     */
    public function setUtilisateur(\PLU\CoreBundle\Entity\Utilisateur $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \PLU\CoreBundle\Entity\Utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->licences = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add licence
     *
     * @param \PLU\CoreBundle\Entity\Licence $licence
     *
     * @return Agenda
     */
    public function addLicence(\PLU\CoreBundle\Entity\Licence $licence)
    {
        $this->licences[] = $licence;

        return $this;
    }

    /**
     * Remove licence
     *
     * @param \PLU\CoreBundle\Entity\Licence $licence
     */
    public function removeLicence(\PLU\CoreBundle\Entity\Licence $licence)
    {
        $this->licences->removeElement($licence);
    }

    /**
     * Get licences
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLicences()
    {
        return $this->licences;
    }
}
