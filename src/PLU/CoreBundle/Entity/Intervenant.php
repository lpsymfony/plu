<?php

namespace PLU\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use PLU\CoreBundle\Entity\Entreprise;

/**
* Intervenant
*
* @ORM\Table(name="plu_intervenant")
* @ORM\Entity(repositoryClass="PLU\CoreBundle\Repository\IntervenantRepository")
*/
class Intervenant extends Utilisateur
{
  /**
  * @var int
  *
  * @ORM\Column(name="id", type="integer")
  * @ORM\Id
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  private $id;

  /**
  * @var string
  *
  * @ORM\Column(name="nom", type="string", length=255)
  * @Assert\NotBlank(message="Ce champ est obligatoire.")
  * @Assert\Length(
  *      max=255,
  *      maxMessage="Votre nom est trop long, il est limité à {{ limit }} caractères."
  * )
  */
  private $nom;

  /**
  * @var string
  *
  * @ORM\Column(name="prenom", type="string", length=255)
  * @Assert\NotBlank(message="Ce champ est obligatoire.")
  * @Assert\Length(
  *      max=255,
  *      maxMessage="Votre prenom est trop long, il est limité à {{ limit }} caractères."
  * )
  */
  private $prenom;

  /**
  * @ORM\ManyToOne(targetEntity="PLU\CoreBundle\Entity\Entreprise")
  * @ORM\JoinColumn(nullable=false, name="entreprise_id", referencedColumnName="id", onDelete="CASCADE")
  * @Assert\NotBlank(message="Ce champ est obligatoire")
  */
  private $entreprise;

  public function __construct() {
      $this->roles = array('ROLE_INTERVENANT');
  }




  /**
  * Set nom
  *
  * @param string $nom
  *
  * @return Intervenant
  */
  public function setNom($nom)
  {
    $this->nom = $nom;

    return $this;
  }

  /**
  * Get nom
  *
  * @return string
  */
  public function getNom()
  {
    return $this->nom;
  }

  /**
  * Set prenom
  *
  * @param string $prenom
  *
  * @return Intervenant
  */
  public function setPrenom($prenom)
  {
    $this->prenom = $prenom;

    return $this;
  }

  /**
  * Get prenom
  *
  * @return string
  */
  public function getPrenom()
  {
    return $this->prenom;
  }

    /**
     * Set entreprise
     *
     * @param \PLU\CoreBundle\Entity\Entreprise $entreprise
     *
     * @return Intervenant
     */
    public function setEntreprise(\PLU\CoreBundle\Entity\Entreprise $entreprise)
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * Get entreprise
     *
     * @return \PLU\CoreBundle\Entity\Entreprise
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }
}
