<?php

namespace PLU\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Etudiant
 *
 * @ORM\Table(name="plu_etudiant")
 * @ORM\Entity(repositoryClass="PLU\CoreBundle\Repository\EtudiantRepository")
 * @UniqueEntity("emailInstit", message="Cette adresse e-mail est déjà utilisée.")
 */
class Etudiant extends Utilisateur
{

    /**
     * @ORM\ManyToOne(targetEntity="PLU\CoreBundle\Entity\Licence", inversedBy="etudiant")
     * @ORM\JoinColumn(name="licence_id", referencedColumnName="id", onDelete="SET NULL")
     * @ORM\JoinColumn(nullable=true)
     */
    private $licence;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     * @Assert\Length(
     *      max=255,
     *      maxMessage="Le nom est limité à {{ limit }} caractères."
     * )
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     * @Assert\Length(
     *      max=255,
     *      maxMessage="Le prénom est limité à {{ limit }} caractères."
     * )
     */
    private $prenom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateNaissance", type="datetime")
     * @Assert\DateTime()
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     * @Assert\LessThan("-15 years", message="Cette valeur doit être inférieur à {{ compared_value }}.")
     */
    private $dateNaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", length=1)
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     * @Assert\Choice({"m","f"})
     */
    private $sexe;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=10, nullable=true)
     * @Assert\Regex(
     *     pattern="/\d{10}/",
     *     message="Le numéro de téléphone doit être composé de 10 chiffres sans caractère ou espace entre eux."
     * )
     * @Assert\Length(
     *      max=10,
     *      maxMessage="Le numéro de téléphone doit être composé de {{ limit }} chiffres sans caractère ou espace entre eux."
     * )
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="typeBac", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     * @Assert\Length(
     *      max=255,
     *      maxMessage="Le type de bac est limité à {{ limit }} caractères."
     * )
     */
    private $typeBac;

    /**
     * @var string
     *
     * @ORM\Column(name="emailInstit", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     * @Assert\Regex(
     *     pattern="/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/",
     *     message = "L'email '{{ value }}' n'est pas une email valide."
     * )
     * @Assert\Length(
     *      max=255,
     *      maxMessage="L'email institutionnelle est limitée à {{ limit }} caractères."
     * )
     */
    private $emailInstit;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="anneeBac", columnDefinition="year", length=4)
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     * @Assert\Choice(callback = "yearsRange")
     */
    private $anneeBac;

    /**
     * @var string
     *
     * @ORM\Column(name="rue", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     * @Assert\Length(
     *      max=255,
     *      maxMessage="La rue est limitée à {{ limit }} caractères."
     * )
     */
    private $rue;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     * @Assert\Length(
     *      max=255,
     *      maxMessage="La ville est limitée à {{ limit }} caractères."
     * )
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="codePostal", type="string", length=5)
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     * @Assert\Regex(
     *     pattern="/\d{5}/",
     *     message="Le code postal doit être composé de 5 chiffres."
     * )
     * @Assert\Length(
     *      max=5,
     *      maxMessage="Le code postal doit être composé de {{ limit }} chiffres."
     * )
     */
    private $codePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="typeBacSup", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      max=255,
     *      maxMessage="Le type de bac +2 est limité à {{ limit }} caractères."
     * )
     */
    private $typeBacSup;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="anneeBacSup", columnDefinition="year", length=4, nullable=true)
     * @Assert\Choice(callback = "yearsRange")
     */
    private $anneeBacSup;

    //--------------------------------------------------------------------------

    public function __construct() {
        $this->roles = array('ROLE_ETUDIANT_FUTUR');
        $this->dateNaissance = new \Datetime();
    }

    //--------------------------------------------------------------------------

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Etudiant
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Etudiant
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set dateNaissance
     *
     * @param \DateTime $dateNaissance
     *
     * @return Etudiant
     */
    public function setDateNaissance($dateNaissance)
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    /**
     * Get dateNaissance
     *
     * @return \DateTime
     */
    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     *
     * @return Etudiant
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Etudiant
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set typeBac
     *
     * @param string $typeBac
     *
     * @return Etudiant
     */
    public function setTypeBac($typeBac)
    {
        $this->typeBac = $typeBac;

        return $this;
    }

    /**
     * Get typeBac
     *
     * @return string
     */
    public function getTypeBac()
    {
        return $this->typeBac;
    }

    /**
     * Set emailInstit
     *
     * @param string $emailInstit
     *
     * @return Etudiant
     */
    public function setEmailInstit($emailInstit)
    {
        $this->emailInstit = $emailInstit;

        return $this;
    }

    /**
     * Get emailInstit
     *
     * @return string
     */
    public function getEmailInstit()
    {
        return $this->emailInstit;
    }

    /**
     * Set anneeBac
     *
     * @param \DateTime $anneeBac
     *
     * @return Etudiant
     */
    public function setAnneeBac($anneeBac)
    {
        $this->anneeBac = $anneeBac;

        return $this;
    }

    /**
     * Get anneeBac
     *
     * @return \DateTime
     */
    public function getAnneeBac()
    {
        return $this->anneeBac;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Etudiant
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set rue
     *
     * @param string $rue
     *
     * @return Etudiant
     */
    public function setRue($rue)
    {
        $this->rue = $rue;

        return $this;
    }

    /**
     * Get rue
     *
     * @return string
     */
    public function getRue()
    {
        return $this->rue;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return Etudiant
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }


    /**
     * Set typeBacSup
     *
     * @param string $typeBacSup
     *
     * @return Etudiant
     */
    public function setTypeBacSup($typeBacSup)
    {
        $this->typeBacSup = $typeBacSup;

        return $this;
    }

    /**
     * Get typeBacSup
     *
     * @return string
     */
    public function getTypeBacSup()
    {
        return $this->typeBacSup;
    }

    /**
     * Set anneeBacSup
     *
     * @param \DateTime $anneeBacSup
     *
     * @return Etudiant
     */
    public function setAnneeBacSup($anneeBacSup)
    {
        $this->anneeBacSup = $anneeBacSup;

        return $this;
    }

    /**
     * Get anneeBacSup
     *
     * @return \DateTime
     */
    public function getAnneeBacSup()
    {
        return $this->anneeBacSup;
    }

    public function yearsRange() {
      $yearArray;
      $date = date("Y");
      for ($i=$date; $i > $date-50; $i--) {
        $yearArray[$i]=$i;
      }

      return $yearArray;
    }


    /**
     * Set licence
     *
     * @param \PLU\CoreBundle\Entity\Licence $licence
     *
     * @return Etudiant
     */
    public function setLicence(\PLU\CoreBundle\Entity\Licence $licence = null)
    {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Get licence
     *
     * @return \PLU\CoreBundle\Entity\Licence
     */
    public function getLicence()
    {
        return $this->licence;
    }
}
