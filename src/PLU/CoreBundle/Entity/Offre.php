<?php

namespace PLU\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="plu_offre")
* @ORM\Entity(repositoryClass="PLU\CoreBundle\Repository\OffreRepository")
* @ORM\HasLifecycleCallbacks
*/
class Offre {

  /**
  * @ORM\ManyToOne(targetEntity="PLU\CoreBundle\Entity\Categorie")
  * @ORM\JoinColumn(nullable=false)
  * @Assert\NotNull(message="Vous devez obligatoirement indiquer une categorie")
  */
  private $categorie;

  /**
  * @ORM\ManyToOne(targetEntity="PLU\CoreBundle\Entity\Entreprise", inversedBy="offres")
  * @ORM\JoinColumn(name="entreprise_id", referencedColumnName="id", onDelete="CASCADE")
  * @Assert\NotNull(message="Vous devez obligatoirement indiquer une entreprise")
  */
  private $entreprise;

  /**
  * @var int
  *
  * @ORM\Column(name="id", type="integer")
  * @ORM\Id
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  private $id;

  /**
  * @var string
  *
  * @ORM\Column(name="intitule", type="string", length=128)
  * @Assert\Length(min=5, minMessage = "Votre titre est trop court. Il doit contenir au minimum {{ limit }} caractères")
  * @Assert\Length(max=128, maxMessage = "Votre titre est trop long. Il doit contenir au maximum {{ limit }} caractères")
  * @Assert\NotBlank(message="Veuillez indiqué l'intitulé de votre annonce")
  */
  private $intitule;

  /**
  * @var string
  *
  * @ORM\Column(name="description", type="text")
  * @Assert\NotBlank()
  */
  private $description;

  /**
  * @var string
  *
  * @ORM\Column(name="fichiers", type="string", length=255, nullable=true)
  * @Assert\Length(max=255, maxMessage = "Votre nom de fichier est trop long. Il doit contenir au maximum {{ limit }} caractères")
  */
  private $fichiers;

  private $file;

  private $tempFilename;

  /**
  * @var string
  *
  * @ORM\Column(name="addr", type="string", length=255)
  * @Assert\NotBlank()
  * @Assert\Length(max=255, maxMessage = "Votre adresse est trop longue. Elle doit contenir au maximum {{ limit }} caractères")
  */
  private $addr;

  /**
  * @var \DateTime
  *
  * @ORM\Column(name="date", type="datetime")
  * @Assert\DateTime()
  */
  private $date;

  /**
  * @var bool
  *
  * @ORM\Column(name="publie", type="boolean")
  */
  private $publie = true;

  /**
  * @var bool
  *
  * @ORM\Column(name="valide", type="boolean")
  */
  private $valide = true;

  //------------------------------------ file --------------------------------

  public function getFile(){
    return $this->file;
  }

  public function setFile(UploadedFile $file = null){
    $this->file = $file;

    if (null !== $this->fichiers) {
      $this->tempFilename = $this->fichiers;

      $this->fichiers = null;
    }
  }

  /**
  * @ORM\PrePersist()
  * @ORM\PreUpdate()
  */
  public function preUpload(){
    if (null === $this->file) {
      return;
    }

    $this->fichiers = $this->file->getClientOriginalName();
  }

  /**
  * @ORM\PostPersist()
  * @ORM\PostUpdate()
  */
  public function upload(){
    if (null === $this->file) {
      return;
    }

    if (null !== $this->tempFilename) {
      $oldFile = $this->getUploadDir().'/'.$this->id.'_'.$this->tempFilename;
      if (file_exists($oldFile)) {
        unlink($oldFile);
      }
      $this->tempFilename = null;
    }

    $this->file->move(
      $this->getUploadRootDir(),
      $this->id.'_'.$this->fichiers
    );
  }

  /**
  * @ORM\PreRemove()
  */
  public function preRemoveUpload(){
    // On sauvegarde temporairement le nom du fichier, car il dépend de l'id
    $this->tempFilename = $this->getUploadDir().'/'.$this->id.'_'.$this->getFichiers();
  }

  /**
  * @ORM\PostRemove()
  */
  public function removeUpload(){
    // En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
    if (file_exists($this->tempFilename)) {
      // On supprime le fichier
      unlink($this->tempFilename);
    }
  }

  public function getUploadDir(){
    // On retourne le chemin relatif vers l'image pour un navigateur
    return 'uploads/img';
  }

  protected function getUploadRootDir(){
    // On retourne le chemin relatif vers l'image pour notre code PHP
    return __DIR__.'/../../../../web/'.$this->getUploadDir();
  }

  public function getWebPath(){
    return $this->getUploadDir().'/'.$this->id.'_'.$this->getFichiers();
  }

  //--------------------------------------------------------------------------

  public function __construct(){
    $this->date = new \Datetime();
  }

  //--------------------------------------------------------------------------

  /**
  * Get id
  *
  * @return int
  */
  public function getId()
  {
    return $this->id;
  }

  /**
  * Set intitule
  *
  * @param string $intitule
  *
  * @return Offre
  */
  public function setIntitule($intitule)
  {
    $this->intitule = $intitule;

    return $this;
  }

  /**
  * Get intitule
  *
  * @return string
  */
  public function getIntitule()
  {
    return $this->intitule;
  }

  /**
  * Set description
  *
  * @param string $description
  *
  * @return Offre
  */
  public function setDescription($description)
  {
    $this->description = $description;

    return $this;
  }

  /**
  * Get description
  *
  * @return string
  */
  public function getDescription()
  {
    return $this->description;
  }

  /**
  * Set fichiers
  *
  * @param string $fichiers
  *
  * @return Offre
  */
  public function setFichiers($fichiers)
  {
    $this->fichiers = $fichiers;

    return $this;
  }

  /**
  * Get fichiers
  *
  * @return string
  */
  public function getFichiers()
  {
    return $this->fichiers;
  }

  /**
  * Set addr
  *
  * @param string $addr
  *
  * @return Offre
  */
  public function setAddr($addr)
  {
    $this->addr = $addr;

    return $this;
  }

  /**
  * Get addr
  *
  * @return string
  */
  public function getAddr()
  {
    return $this->addr;
  }

  /**
  * Set date
  *
  * @param \DateTime $date
  *
  * @return Offre
  */
  public function setDate($date)
  {
    $this->date = $date;

    return $this;
  }

  /**
  * Get date
  *
  * @return \DateTime
  */
  public function getDate()
  {
    return $this->date;
  }

  /**
  * Set publie
  *
  * @param boolean $publie
  *
  * @return Offre
  */
  public function setPublie($publie)
  {
    $this->publie = $publie;

    return $this;
  }

  /**
  * Get valide
  *
  * @return bool
  */
  public function getValide()
  {
    return $this->valide;
  }

  /**
  * Set valide
  *
  * @param boolean $valide
  *
  * @return Offre
  */
  public function setValide($valide)
  {
    $this->valide = $valide;

    return $this;
  }

  /**
  * Get publie
  *
  * @return bool
  */
  public function getPublie()
  {
    return $this->publie;
  }

  //--------------------------------------------------------------------------

  public function setCategorie(Categorie $categorie = null){
    $this->categorie = $categorie;
    // $this->categorie = $categorie->getId();
  }

  public function getCategorie(){
    return $this->categorie;
  }

  public function setEntreprise(Entreprise $entreprise = null){
    $this->entreprise = $entreprise;
  }

  public function getEntreprise(){
    return $this->entreprise;
  }

}
