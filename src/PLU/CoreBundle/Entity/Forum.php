<?php

namespace PLU\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Forum
 *
 * @ORM\Table(name="plu_forum")
 * @ORM\Entity(repositoryClass="PLU\CoreBundle\Repository\ForumRepository")
 * @UniqueEntity(fields="titre", message="Ce titre de forum existe déjà")
 */
class Forum
{

    /**
    * @ORM\ManyToOne(targetEntity="PLU\CoreBundle\Entity\Sujet")
    * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
    * @Assert\NotNull(message="Vous devez sélectionner un sujet")
    */
    private $sujet;

    /**
    * @ORM\ManyToOne(targetEntity="PLU\CoreBundle\Entity\Utilisateur")
    * @ORM\JoinColumn(onDelete="SET NULL")
    */
    private $createur;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, unique=true)
    * @Assert\NotNull(message="Ce champ est obligatoire")
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
    * @Assert\NotNull(message="Ce champ est obligatoire")
     */
    private $description;

    /**
    * @var \DateTime
    *
    * @ORM\Column(name="date", type="datetime")
    * @Assert\DateTime()
    */
    private $date;

    /**
     * @var bool
     *
     * @ORM\Column(name="resolu", type="boolean")
     */
    private $resolu = false;

    //--------------------------------------------------------------------------

    public function __construct(){
      $this->date = new \Datetime();
    }

    //--------------------------------------------------------------------------

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Forum
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Forum
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Forum
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set sujet
     *
     * @param integer $sujet
     *
     * @return \PLU\CoreBundle\Entity\Sujet $sujet
     */
    public function setSujet($sujet)
    {
        $this->sujet = $sujet;

        return $this;
    }

    /**
     * Get sujet
     *
     * @return \PLU\CoreBundle\Entity\Sujet $sujet
     */
    public function getSujet()
    {
        return $this->sujet;
    }

    /**
     * Set createur
     *
     * @param \PLU\CoreBundle\Entity\Utilisateur $createur
     *
     * @return Forum
     */
    public function setCreateur($createur)
    {
        $this->createur = $createur;

        return $this;
    }

    /**
     * Get createur
     *
     * @return \PLU\CoreBundle\Entity\Utilisateur $createur
     */
    public function getCreateur()
    {
        return $this->createur;
    }

    /**
     * Set resolu
     *
     * @param boolean $resolu
     *
     * @return Forum
     */
    public function setResolu($resolu)
    {
        $this->resolu = $resolu;

        return $this;
    }

    /**
     * Get resolu
     *
     * @return boolean
     */
    public function getResolu()
    {
        return $this->resolu;
    }
}
