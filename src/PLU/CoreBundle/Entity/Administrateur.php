<?php

namespace PLU\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Administrateur
 *
 * @ORM\Table(name="plu_administrateur")
 * @ORM\Entity(repositoryClass="PLU\CoreBundle\Repository\AdministrateurRepository")
 */
class Administrateur extends Utilisateur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     * @Assert\Length(
     *      max=255,
     *      maxMessage="Le nom est limité à {{ limit }} caractères."
     * )
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire.")
     * @Assert\Length(
     *      max=255,
     *      maxMessage="Le prénom est limité à {{ limit }} caractères."
     * )
     */
    private $prenom;

    //--------------------------------------------------------------------------

    public function __construct() {
        $this->roles = array('ROLE_ADMINISTRATEUR');
    }

    //--------------------------------------------------------------------------

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Administrateur
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Administrateur
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }
}
