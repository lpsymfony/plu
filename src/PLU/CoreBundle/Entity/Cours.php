<?php

namespace PLU\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Table(name="plu_Cours")
* @ORM\Entity(repositoryClass="PLU\CoreBundle\Repository\CoursRepository")
* @ORM\HasLifecycleCallbacks
*/
class Cours {

  /**
  * @ORM\ManyToMany(targetEntity="PLU\CoreBundle\Entity\Licence", inversedBy="cours")
  * @ORM\JoinTable(
     *  name="plu_cours_licences",
     *  joinColumns={
     *      @ORM\JoinColumn(name="cours_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="licence_id", referencedColumnName="id")
     *  }
     * )
  */
  private $licences;

  /**
   * @ORM\ManyToOne(targetEntity="PLU\CoreBundle\Entity\Utilisateur")
   * @ORM\JoinColumn(name="utilisateur_id", referencedColumnName="id", onDelete="SET NULL")
   */
  private $utilisateur;

  /**
  * @var int
  *
  * @ORM\Column(name="id", type="integer")
  * @ORM\Id
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  private $id;

  /**
  * @var string
  *
  * @ORM\Column(name="description", type="text", nullable=true)
  */
  private $description;

  /**
  * @var string
  *
  * @ORM\Column(name="fichiers", type="string", length=255, nullable=true)
  * @Assert\Length(max=255, maxMessage = "Votre nom de fichier est trop long. Il doit contenir au maximum {{ limit }} caractères")
  */
  private $fichiers;

  /**
  * @var string
  *
  * @ORM\Column(name="chemin", type="text")
  * @Assert\NotBlank(message="Le chemin ne peut pas être vide.")
  */
  private $chemin;

  /**
  * @var string
  *
  * @ORM\Column(name="visible", type="boolean")
  */
  private $visible;

  private $file;

  private $tempFilename;

  private $oldPath;

  private $oldId;

  private $oldFichiers;

  private $folderList;

  //------------------------------------ file --------------------------------

  public function getTempFilename(){
    return $this->tempFilename;
  }

  public function getOldPath(){
    return $this->oldPath;
  }

  public function setOldPath($old){
    $this->oldPath = $old;
  }

  public function getOldId(){
    return $this->oldId;
  }

  public function setOldId($old){
    $this->oldId = $old;
  }

  public function getOldFichiers(){
    return $this->oldFichiers;
  }

  public function setOldFichiers($old){
    $this->oldFichiers = $old;
  }

  public function getFile(){
    return $this->file;
  }

  public function setFile(UploadedFile $file = null){
    $this->file = $file;

    if (null !== $this->fichiers) {
      $this->tempFilename = $this->fichiers;

      $this->fichiers = null;
    }
  }

  /**
  * @ORM\PrePersist()
  * @ORM\PreUpdate()
  */
  public function preUpload(){
    if (null === $this->file) {
      return;
    }

    $this->fichiers = $this->file->getClientOriginalName();
  }

  /**
  * @ORM\PostPersist()
  * @ORM\PostUpdate()
  */
  public function upload(){
    if (null !== $this->oldId && null !== $this->oldPath) {
      $path = $this->getOldUploadRootDir($this->oldPath);
      $this->file = new File($path."/".$this->oldId.'_'.$this->fichiers);
    }

    if (null === $this->file) {
      return;
    }

    if (null !== $this->tempFilename) {
      $oldFile = $this->getUploadDir().'/'.$this->id.'_'.$this->tempFilename;
      if (file_exists($oldFile)) {
        unlink($oldFile);
      }
      $this->tempFilename = null;
    }

    $this->file->move(
      $this->getUploadRootDir(),
      $this->id.'_'.$this->fichiers
    );
  }

  /**
  * @ORM\PreRemove()
  */
  public function preRemoveUpload(){
    // On sauvegarde temporairement le nom du fichier, car il dépend de l'id
    if (null !== $this->oldId && null !== $this->oldPath && null !== $this->oldFichiers) {
      $this->tempFilename = 'cours/'.$this->oldPath.'/'.$this->oldId.'_'.$this->oldFichiers;
    } else {
      $this->tempFilename = $this->getUploadDir().'/'.$this->id.'_'.$this->getFichiers();
    }
  }

  /**
  * @ORM\PostRemove()
  */
  public function removeUpload(){
    // En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
    if (file_exists($this->tempFilename)) {
      // On supprime le fichier
      unlink($this->tempFilename);
    }
  }

  public function createRootDirIfDoesntExist () {
    $fs = new Filesystem();
    if (!$fs->exists($this->getWebDir().'cours')) {
       $fs->mkdir($this->getWebDir().'cours');
    }
  }

  public function getUploadDir(){
    // On retourne le chemin relatif vers l'image pour un navigateur
    return 'cours/'.$this->chemin;
  }

  protected function getUploadRootDir(){
    // On retourne le chemin relatif vers l'image pour notre code PHP
    return __DIR__.'/../../../../web/'.$this->getUploadDir();
  }

  protected function getWebDir(){
    // On retourne le chemin relatif vers l'image pour notre code PHP
    return __DIR__.'/../../../../web/';
  }

  public function getWebPath(){
    return $this->getUploadDir().'/'.$this->id.'_'.$this->getFichiers();
  }

  protected function getOldUploadRootDir($oldPath){
    // On retourne le chemin relatif vers l'image pour notre code PHP
    return __DIR__.'/../../../../web/cours/'.$oldPath;
  }
  
  public function dirToArray($dir, $listCours) { 
    $result = array(); 
    $cdir = scandir($dir); 
    foreach ($cdir as $key => $value) 
    { 
      if (!in_array($value,array(".",".."))) 
      { 
        if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
        { 
          $explodeResult = explode(DIRECTORY_SEPARATOR, $dir . DIRECTORY_SEPARATOR . $value);
          unset($explodeResult[0]);
          $key = implode(DIRECTORY_SEPARATOR, $explodeResult);
          $result[$key] = $this->dirToArray($dir . DIRECTORY_SEPARATOR . $value, $listCours); 
        } 
        else 
        {
          $fileId = explode("_", $value)[0];
          if (null !== $fileId 
              && array_key_exists($fileId, $listCours) 
              && null !== $listCours[$fileId]) 
          {
            $result[] = $listCours[$fileId]; 
          } 
        } 
      } 
    } 
    return $result; 
  } 

  public function getListFolder($dir){
    $value = ltrim($dir, "cours/");
    if ($dir !== "cours") {
      $this->folderList[$value."/"] = $value;
    } else {
      $this->folderList["/"] = "";
    }
    
    $ffs = scandir($dir);
    unset($ffs[array_search('.', $ffs, true)]);
    unset($ffs[array_search('..', $ffs, true)]);

    // prevent empty ordered elements
    if (count($ffs) < 1)
      return;

    foreach($ffs as $ff){
      if(is_dir($dir.'/'.$ff)) {
        $this->getListFolder($dir.'/'.$ff);
      }
    }
    return $this->folderList;
  }
 

  public function coursArrayToAssociativeArray($listCours) {
    $AssociativeArray = Array();
    foreach($listCours as $key => $value)
    {
      $AssociativeArray[$value->getId()] = $value;
    }
    return $AssociativeArray;
  }

  public function createFolder ($path) {
    $fs = new Filesystem();
    $path = "cours/".$path;
    if (!$fs->exists($path)) {
      try {
        $fs->mkdir($path);
        return true;
      } catch (IOExceptionInterface $e) {
        echo "Une erreur est survenue lors de la creation du dossier à ".$e->getPath();
      }
    } else {
      return false;
    }
  }

  public function deleteFolder ($path) {
    $fs = new Filesystem();
    $path = "cours/".$path;
    if ($fs->exists($path)) {
      try {
        $fs->remove($path);
        return true;
      } catch (IOExceptionInterface $e) {
        echo "Une erreur est survenue lors de la suppression du dossier à ".$e->getPath();
      }
    } else {
      return false;
    }
  }

  public function editFolder ($oldPath, $newPath) {
    $fs = new Filesystem();
    $oldPath = "cours/".$oldPath;
    if ($fs->exists($oldPath)) {
      try {
        $fs->rename($oldPath, "cours/".$newPath, true);
        return true;
      } catch (IOExceptionInterface $e) {
        echo "Une erreur est survenue lors de la suppression du dossier à ".$e->getPath();
      }
    } else {
      return false;
    }
  }

  //--------------------------------------------------------------------------

  public function __construct(){
    $this->folderList = array();
    $this->date = new \Datetime();
  }

  //--------------------------------------------------------------------------

  /**
  * @Assert\IsTrue(message="Le chemin ne peut pas contenir de points")
  */
  public function isCheminValid()
  {
    return strpos($this->chemin, '.')===false;
  }

  /**
  * Get id
  *
  * @return int
  */
  public function getId()
  {
    return $this->id;
  }

  /**
  * Set description
  *
  * @param string $description
  *
  * @return Cours
  */
  public function setDescription($description)
  {
    $this->description = $description;

    return $this;
  }

  /**
  * Get description
  *
  * @return string
  */
  public function getDescription()
  {
    return $this->description;
  }

  /**
  * Set fichiers
  *
  * @param string $fichiers
  *
  * @return Cours
  */
  public function setFichiers($fichiers)
  {
    $this->fichiers = $fichiers;

    return $this;
  }

  /**
  * Get fichiers
  *
  * @return string
  */
  public function getFichiers()
  {
    return $this->fichiers;
  }

  /**
   * Add licence
   *
   * @param \PLU\CoreBundle\Entity\Licence $licence
   *
   * @return Cours
   */
  public function addLicence(\PLU\CoreBundle\Entity\Licence $licence)
  {
      $this->licences[] = $licence;

      return $this;
  }

  /**
   * Remove licence
   *
   * @param \PLU\CoreBundle\Entity\Licence $licence
   */
  public function removeLicence(\PLU\CoreBundle\Entity\Licence $licence)
  {
      $this->licences->removeElement($licence);
  }

  /**
   * Get licences
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getLicences()
  {
      return $this->licences;
  }

  public function getLicencesArray() {
    foreach ($this->getLicences() as $key => $value) {
      $licencesArray[$value->getId()] = $value->getId();
    }
    return $licencesArray;
  }

  /**
   * Set utilisateur
   *
   * @param \PLU\CoreBundle\Entity\Utilisateur $utilisateur
   *
   * @return Cours
   */
  public function setUtilisateur(\PLU\CoreBundle\Entity\Utilisateur $utilisateur = null)
  {
      $this->utilisateur = $utilisateur;

      return $this;
  }

  /**
   * Get utilisateur
   *
   * @return \PLU\CoreBundle\Entity\Utilisateur
   */
  public function getUtilisateur()
  {
      return $this->utilisateur;
  }

    /**
     * Set chemin
     *
     * @param string $chemin
     *
     * @return Cours
     */
    public function setChemin($chemin)
    {
        $this->chemin = $chemin;

        return $this;
    }

    /**
     * Get chemin
     *
     * @return string
     */
    public function getChemin()
    {
        return $this->chemin;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return Cours
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return string
     */
    public function getVisible()
    {
        return $this->visible;
    }
}
