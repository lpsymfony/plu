<?php

namespace PLU\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Utilisateur
 *
 * @ORM\Table(name="plu_utilisateur")
 * @UniqueEntity(fields={"username"}, repositoryMethod="findByUsername", message="Cet identidiant est déjà utilisé.")
 * @ORM\Entity(repositoryClass="PLU\CoreBundle\Repository\UtilisateurRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap(
 *    {
 *      "etudiant" = "Etudiant",
 *      "entreprise" = "Entreprise",
 *      "enseignant" = "Enseignant",
 *      "administrateur" = "Administrateur",
 *      "intervenant" = "Intervenant"
 *    }
 * )
 */
abstract class Utilisateur implements UserInterface
{

  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="username", type="string", length=255, unique=true)
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   * @Assert\Regex(
   *     pattern="/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/",
   *     message = "L'email '{{ value }}' n'est pas une email valide."
   * )
   * @Assert\Length(
   *      max=255,
   *      maxMessage="L'email est limitée à {{ limit }} caractères."
   * )
   */
  protected $username;

  /**
   * @var string
   *
   * @ORM\Column(name="password", type="string", length=255)
   * @Assert\NotBlank(message="Ce champ est obligatoire")
   * @Assert\Length(
   *      max=255,
   *      maxMessage="Le mot de passe est limité à {{ limit }} caractères."
   * )
   */
  protected $password;

  /**
   * @var string
   *
   * @Assert\Length(
   *      max=255,
   *      maxMessage="Le mot de passe est limité à {{ limit }} caractères."
   * )
   */
  protected $checkPassword;

  protected $oldPassword;

  /**
   * @ORM\Column(name="salt", type="string", length=255)
   */
  protected $salt;

  /**
   * @var array
   *
   * @ORM\Column(name="roles", type="array")
   */
  protected $roles;

  /**
   * @var string
   *
   * @ORM\Column(name="keyreset", type="string", length=40, nullable=true)
   */
  protected $keyreset;

  //------------------------------------------------------------------------------

  public function eraseCredentials(){}

  //------------------------------------------------------------------------------

  /**
   * Get id
   *
   * @return int
   */
  public function getId(){
    return $this->id;
  }

  /**
   * Set username
   *
   * @param string $username
   *
   * @return Utilisateur
   */
  public function setUsername($username){
    $this->username = $username;

    return $this;
  }

  /**
   * Get username
   *
   * @return string
   */
  public function getUsername(){
    return $this->username;
  }

  /**
   * Set password
   *
   * @param string $password
   *
   * @return Utilisateur
   */
  public function setPassword($password){
    $this->password = $password;

    return $this;
  }

  /**
   * Get password
   *
   * @return string
   */
  public function getPassword(){
    return $this->password;
  }

  /**
   * Set checkPassword
   *
   * @param string $checkPassword
   *
   * @return Utilisateur
   */
  public function setCheckPassword($checkPassword){
    $this->checkPassword = $checkPassword;

    return $this;
  }

  /**
   * Get checkPassword
   *
   * @return string
   */
  public function getCheckPassword(){
    return $this->checkPassword;
  }

  /**
   * Set oldPassword
   *
   * @param string $oldPassword
   *
   * @return Utilisateur
   */
  public function setOldPassword($oldPassword){
    $this->oldPassword = $oldPassword;

    return $this;
  }

  /**
   * Get oldPassword
   *
   * @return string
   */
  public function getOldPassword(){
    return $this->oldPassword;
  }

  /**
   * Set salt
   *
   * @param string $salt
   *
   * @return Utilisateur
   */
  public function setSalt($salt){
    $this->salt = $salt;

    return $this;
  }

  /**
   * Get salt
   *
   * @return string
   */
  public function getSalt(){
    return $this->salt;
  }

  /**
   * Set roles
   *
   * @param array $roles
   *
   * @return Utilisateur
   */
  public function setRoles($roles){
    $this->roles = $roles;

    return $this;
  }

  /**
   * Get roles
   *
   * @return array
   */
  public function getRoles(){
    return $this->roles;
  }
/**
   * Set keyreset
   *
   * @param string $keyreset
   *
   * @return Utilisateur
   */
  public function setKeyreset($keyreset){
    $this->keyreset = $keyreset;

    return $this;
  }

  /**
   * Get keyreset
   *
   * @return string
   */
  public function getKeyreset(){
    return $this->keyreset;
  }

  public function getClassName()
  {
      return (new \ReflectionClass($this))->getShortName();
  }

  //------------------------------------------------------------------------------



}
