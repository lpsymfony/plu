<?php

namespace PLU\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
* Licence
*
* @ORM\Table(name="plu_licence")
* @ORM\Entity(repositoryClass="PLU\CoreBundle\Repository\LicenceRepository")
* @UniqueEntity("nom", message="Ce nom de licence est déjà utilisé.")
*/
class Licence
{
	/**
	* @ORM\ManyToOne(targetEntity="PLU\CoreBundle\Entity\Enseignant", cascade={"persist"})
	* @ORM\JoinColumn(nullable=true)
	*/
	private $responsable;

	/**
	* @ORM\OneToMany(targetEntity="PLU\CoreBundle\Entity\Etudiant", mappedBy="licence")
	*/
	private $etudiant;

	/**
	* @ORM\ManyToMany(targetEntity="PLU\CoreBundle\Entity\Agenda", mappedBy="licences",  cascade={"persist"})
	*/
	private $agendas;

	/**
	* @var \Doctrine\Common\Collections\Collection|Enseignant[]
	*
	* @ORM\ManyToMany(targetEntity="PLU\CoreBundle\Entity\Enseignant", mappedBy="licences", cascade={"persist"})
	*/
	private $enseignants;

	/**
	* @var \Doctrine\Common\Collections\Collection|Cours[]
	*
	* @ORM\ManyToMany(targetEntity="PLU\CoreBundle\Entity\Cours", mappedBy="licences", cascade={"persist"})
	*/
	private $cours;

	/**
	* @var int
	*
	* @ORM\Column(name="id", type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	private $id;

	/**
	* @var string
	*
	* @ORM\Column(name="nom", type="string", length=255, unique=true)
	* @Assert\NotBlank(message="Ce champ est obligatoire.")
	* @Assert\Length(
	*      max=255,
	*      maxMessage="Le nom est limitée à {{ limit }} caractères"
	* )
	*/
	private $nom;

	/**
	* @var string
	*
	* @ORM\Column(name="infoGeneral", type="text")
	* @Assert\NotBlank(message="Ce champ est obligatoire.")
	*/
	private $infoGeneral;

	/**
	* Get id
	*
	* @return int
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	* Set nom
	*
	* @param string $nom
	*
	* @return Licence
	*/
	public function setNom($nom)
	{
		$this->nom = $nom;

		return $this;
	}

	/**
	* Get nom
	*
	* @return string
	*/
	public function getNom()
	{
		return $this->nom;
	}

	/**
	* Set infoGeneral
	*
	* @param string $infoGeneral
	*
	* @return Licence
	*/
	public function setInfoGeneral($infoGeneral)
	{
		$this->infoGeneral = $infoGeneral;

		return $this;
	}

	/**
	* Get infoGeneral
	*
	* @return string
	*/
	public function getInfoGeneral()
	{
		return $this->infoGeneral;
	}

	/**
	* Set responsable
	*
	* @param \PLU\CoreBundle\Entity\Enseignant $responsable
	*
	* @return Licence
	*/
	public function setResponsable($responsable)
	{
		$this->responsable = $responsable;

		return $this;
	}

	/**
	* Get responsable
	*
	* @return \PLU\CoreBundle\Entity\Enseignant $responsable
	*/
	public function getResponsable()
	{
		return $this->responsable;
	}

	/**
	* Constructor
	*/
	public function __construct()
	{
		$this->enseignants = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	* Add enseignant
	*
	* @param \PLU\CoreBundle\Entity\Enseignant $enseignant
	*
	* @return Licence
	*/
	public function addEnseignants(\PLU\CoreBundle\Entity\Enseignant $enseignants)
	{
		$this->enseignants[] = $enseignants;

		return $this;
	}

	/**
	* Remove enseignant
	*
	* @param \PLU\CoreBundle\Entity\Enseignant $enseignant
	*/
	public function removeEnseignants(\PLU\CoreBundle\Entity\Enseignant $enseignants)
	{
		$this->enseignants->removeElement($enseignants);
	}

	/**
	* Get enseignant
	*
	* @return \Doctrine\Common\Collections\Collection
	*/
	public function getEnseignants()
	{
		return $this->enseignants;
	}

	/**
	* Add enseignant
	*
	* @param \PLU\CoreBundle\Entity\Enseignant $enseignant
	*
	* @return Licence
	*/
	public function addEnseignant(\PLU\CoreBundle\Entity\Enseignant $enseignant)
	{
		$this->enseignants[] = $enseignant;

		return $this;
	}

	/**
	* Remove enseignant
	*
	* @param \PLU\CoreBundle\Entity\Enseignant $enseignant
	*/
	public function removeEnseignant(\PLU\CoreBundle\Entity\Enseignant $enseignant)
	{
		$this->enseignants->removeElement($enseignant);
	}

	/**
	* Add etudiant
	*
	* @param \PLU\CoreBundle\Entity\Etudiant $etudiant
	*
	* @return Licence
	*/
	public function addEtudiant(\PLU\CoreBundle\Entity\Etudiant $etudiant)
	{
		$this->etudiant[] = $etudiant;

		return $this;
	}

	/**
	* Remove etudiant
	*
	* @param \PLU\CoreBundle\Entity\Etudiant $etudiant
	*/
	public function removeEtudiant(\PLU\CoreBundle\Entity\Etudiant $etudiant)
	{
		$this->etudiant->removeElement($etudiant);
	}

	/**
	* Get etudiant
	*
	* @return \Doctrine\Common\Collections\Collection
	*/
	public function getEtudiant()
	{
		return $this->etudiant;
	}

	/**
	 * Add agenda
	 *
	 * @param \PLU\CoreBundle\Entity\Agenda $agenda
	 *
	 * @return Licence
	 */
	public function addAgenda(\PLU\CoreBundle\Entity\Agenda $agenda)
	{
			$this->agenda[] = $agenda;

			return $this;
	}

	/**
	 * Remove agenda
	 *
	 * @param \PLU\CoreBundle\Entity\Agenda $agenda
	 */
	public function removeAgenda(\PLU\CoreBundle\Entity\Agenda $agenda)
	{
			$this->agenda->removeElement($agenda);
	}

	/**
	 * Add cour
	 *
	 * @param \PLU\CoreBundle\Entity\Cours $cour
	 *
	 * @return Licence
	 */
	public function addCour(\PLU\CoreBundle\Entity\Cours $cour)
	{
			$this->cours[] = $cour;

			return $this;
	}

	/**
	 * Remove cour
	 *
	 * @param \PLU\CoreBundle\Entity\Cours $cour
	 */
	public function removeCour(\PLU\CoreBundle\Entity\Cours $cour)
	{
			$this->cours->removeElement($cour);
	}

	/**
	 * Get cours
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getCours()
	{
			return $this->cours;
	}
}
