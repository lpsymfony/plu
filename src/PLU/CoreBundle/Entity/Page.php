<?php

namespace PLU\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Page
 *
 * @ORM\Table(name="plu_page")
 * @ORM\Entity(repositoryClass="PLU\CoreBundle\Repository\PageRepository")
 */
class Page
{
  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="intitule", type="string", length=128, unique=true)
   *
   * @Assert\Length(min=5, minMessage = "Votre titre est trop court. Il doit contenir au minimum {{ limit }} caractères")
   * @Assert\Length(max=128, maxMessage = "Votre titre est trop long. Il doit contenir au maximum {{ limit }} caractères")
   * @Assert\NotBlank(message="Veuillez indiqué l'intitulé de votre page")
   */
  private $intitule;

  /**
   * @var string
   *
   * @ORM\Column(name="description", type="text")
   *
   * @Assert\NotBlank(message="Veuillez indiqué un description")
   */
  private $description;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="date", type="datetime")
   *
   * @Assert\DateTime()
   */
  private $date;

  /**
   * @var bool
   *
   * @ORM\Column(name="visible", type="boolean", nullable=true)
   */
  private $visible;

  /**
   * @var array
   *
   * @ORM\Column(name="roles", type="array")
   */
  private $roles;

  private $allRoles;

  //--------------------------------------------------------------------------

  public function __construct(){
    $this->date = new \Datetime();
    $this->visible = true;
  }

  //--------------------------------------------------------------------------

  /**
   * Get id
   *
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set intitule
   *
   * @param string $intitule
   *
   * @return Page
   */
  public function setIntitule($intitule)
  {
    $this->intitule = $intitule;

    return $this;
  }

  /**
   * Get intitule
   *
   * @return string
   */
  public function getIntitule()
  {
    return $this->intitule;
  }

  /**
   * Set description
   *
   * @param string $description
   *
   * @return Page
   */
  public function setDescription($description)
  {
    $this->description = $description;

    return $this;
  }

  /**
   * Get description
   *
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Set date
   *
   * @param \DateTime $date
   *
   * @return Page
   */
  public function setDate($date)
  {
    $this->date = $date;

    return $this;
  }

  /**
   * Get date
   *
   * @return \DateTime
   */
  public function getDate()
  {
    return $this->date;
  }

  /**
   * Set visible
   *
   * @param boolean $visible
   *
   * @return Page
   */
  public function setVisible($visible)
  {
    $this->visible = $visible;

    return $this;
  }

  /**
   * Get visible
   *
   * @return bool
   */
  public function getVisible()
  {
    return $this->visible;
  }

  /**
   * Set roles
   *
   * @param array $roles
   *
   * @return Page
   */
  public function setRoles($roles)
  {
      $this->roles = $roles;

      return $this;
  }

  /**
   * Get roles
   *
   * @return array
   */
  public function getRoles()
  {
      return $this->roles;
  }

  /**
     * Set allRoles
     *
     * @param array $allRoles
     *
     * @return Page
     */
    public function setAllRoles($allRoles)
    {
        $this->allRoles = $allRoles;

        return $this;
    }

    /**
     * Get allRoles
     *
     * @return array
     */
    public function getAllRoles()
    {
        return $this->allRoles;
    }

}
