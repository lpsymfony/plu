<?php

namespace PLU\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Sujet
 *
 * @ORM\Table(name="plu_sujet")
 * @ORM\Entity(repositoryClass="PLU\CoreBundle\Repository\SujetRepository")
 * @UniqueEntity(fields="nom", message="Ce sujet existe déjà")
 */
class Sujet
{

    /**
    * @ORM\ManyToOne(targetEntity="PLU\CoreBundle\Entity\Utilisateur")
    * @ORM\JoinColumn(onDelete="SET NULL")
    */
    private $createur;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=128, unique=true)
     * @Assert\NotNull(message="Ce champ est obligatoire")
     */
    private $nom;

    /**
     * @var bool
     *
     * @ORM\Column(name="accepte", type="boolean")
     */
    private $accepte = false;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Sujet
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set acceptee
     *
     * @param boolean $acceptee
     *
     * @return Sujet
     */
    public function setAcceptee($acceptee)
    {
        $this->acceptee = $acceptee;

        return $this;
    }

    /**
     * Get acceptee
     *
     * @return bool
     */
    public function getAcceptee()
    {
        return $this->acceptee;
    }

    /**
     * Set createur
     *
     * @param \PLU\CoreBundle\Entity\Utilisateur $createur
     *
     * @return Sujet
     */
    public function setCreateur(Utilisateur $createur = null)
    {
        $this->createur = $createur;

        return $this;
    }

    /**
     * Get createur
     *
     * @return \PLU\CoreBundle\Entity\Utilisateur $createur
     */
    public function getCreateur()
    {
        return $this->createur;
    }

    /**
     * Set accepte
     *
     * @param boolean $accepte
     *
     * @return Sujet
     */
    public function setAccepte($accepte)
    {
        $this->accepte = $accepte;

        return $this;
    }

    /**
     * Get accepte
     *
     * @return boolean
     */
    public function getAccepte()
    {
        return $this->accepte;
    }
}
