<?php

namespace PLU\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Entreprise
 *
 * @ORM\Table(name="plu_entreprise")
 * @ORM\Entity(repositoryClass="PLU\CoreBundle\Repository\EntrepriseRepository")
 * @UniqueEntity("addr", message="Cette adresse est déjà utilisée.")
 */
class Entreprise extends Utilisateur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=32)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="addr", type="string", length=255, unique=true)
     */
    private $addr;

    /**
    * @ORM\OneToMany(targetEntity="PLU\CoreBundle\Entity\Offre", mappedBy="entreprise",  cascade={"remove"})
    */
    private $offres;

    //--------------------------------------------------------------------------

    public function __construct() {
        $this->roles = array('ROLE_ENTREPRISE');
    }

    //--------------------------------------------------------------------------

    

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Entreprise
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set addr
     *
     * @param string $addr
     *
     * @return Entreprise
     */
    public function setAddr($addr)
    {
        $this->addr = $addr;

        return $this;
    }

    /**
     * Get addr
     *
     * @return string
     */
    public function getAddr()
    {
        return $this->addr;
    }

    /**
     * Add offre
     *
     * @param \PLU\CoreBundle\Entity\Offre $offre
     *
     * @return Entreprise
     */
    public function addOffre(\PLU\CoreBundle\Entity\Offre $offre)
    {
        $this->offres[] = $offre;

        return $this;
    }

    /**
     * Remove offre
     *
     * @param \PLU\CoreBundle\Entity\Offre $offre
     */
    public function removeOffre(\PLU\CoreBundle\Entity\Offre $offre)
    {
        $this->offres->removeElement($offre);
    }

    /**
     * Get offres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffres()
    {
        return $this->offres;
    }
}
