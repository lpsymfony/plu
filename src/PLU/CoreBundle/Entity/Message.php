<?php

namespace PLU\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Message
 *
 * @ORM\Table(name="plu_message")
 * @ORM\Entity(repositoryClass="PLU\CoreBundle\Repository\MessageRepository")
 */
class Message
{

    /**
    * @ORM\ManyToOne(targetEntity="PLU\CoreBundle\Entity\Forum")
    * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
    */
    private $forum;

    /**
    * @ORM\ManyToOne(targetEntity="PLU\CoreBundle\Entity\Message")
    * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
    */
    private $message;

    /**
    * @ORM\ManyToOne(targetEntity="PLU\CoreBundle\Entity\Utilisateur")
    * @ORM\JoinColumn(onDelete="SET NULL")
    */
    private $utilisateur;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text")
     * @Assert\NotNull(message="Veuiller écrire une réponse")
     */
    private $contenu;

    /**
    * @var \DateTime
    *
    * @ORM\Column(name="date", type="datetime")
    * @Assert\DateTime()
    */
    private $date;

    //--------------------------------------------------------------------------

    public function __construct(){
      $this->date = new \Datetime();
    }

    //--------------------------------------------------------------------------

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return Message
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Message
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set forum
     *
     * @param \PLU\CoreBundle\Entity\Forum $forum
     *
     * @return Message
     */
    public function setForum($forum)
    {
        $this->forum = $forum;

        return $this;
    }

    /**
     * Get forum
     *
     * @return \PLU\CoreBundle\Entity\Forum $forum
     */
    public function getForum()
    {
        return $this->forum;
    }

    /**
     * Set utilisateur
     *
     * @param \PLU\CoreBundle\Entity\Utilisateur $utilisateur
     *
     * @return Message
     */
    public function setUtilisateur($utilisateur)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \PLU\CoreBundle\Entity\Utilisateur $utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set message
     *
     * @param \PLU\CoreBundle\Entity\Message $message
     *
     * @return Message
     */
    public function setMessage(\PLU\CoreBundle\Entity\Message $message = null)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return \PLU\CoreBundle\Entity\Message
     */
    public function getMessage()
    {
        return $this->message;
    }
}
