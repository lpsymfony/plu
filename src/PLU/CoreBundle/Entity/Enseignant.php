<?php

namespace PLU\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
* Enseignant
*
* @ORM\Table(name="plu_enseignant")
* @ORM\Entity(repositoryClass="PLU\CoreBundle\Repository\EnseignantRepository")
*/
class Enseignant extends Utilisateur
{

  /**
  * @ORM\ManyToMany(targetEntity="PLU\CoreBundle\Entity\Licence", inversedBy="enseignants")
  * @ORM\JoinTable(
     *  name="plu_responsable",
     *  joinColumns={
     *      @ORM\JoinColumn(name="enseignant_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="licence_id", referencedColumnName="id")
     *  }
     * )
  */
  private $licences;

  /**
  * @var int
  *
  * @ORM\Column(name="id", type="integer")
  * @ORM\Id
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  private $id;

  /**
  * @var string
  *
  * @ORM\Column(name="nom", type="string", length=128)
  *
  * @Assert\NotBlank(message="Ce champ est obligatoire")
  * @Assert\Length(
  *      max=128,
  *      maxMessage="Le nom est limité à {{ limit }} caractères."
  * )
  */
  private $nom;

  /**
  * @var string
  *
  * @ORM\Column(name="prenom", type="string", length=128)
  *
  * @Assert\NotBlank(message="Ce champ est obligatoire")
  * @Assert\Length(
  *      max=128,
  *      maxMessage="Le prenom est limité à {{ limit }} caractères."
  * )
  */
  private $prenom;

  //--------------------------------------------------------------------------

  public function __construct() {
    $this->roles = array('ROLE_ENSEIGNANT');
    $this->licences = new \Doctrine\Common\Collections\ArrayCollection();
  }

  //--------------------------------------------------------------------------

  /**
  * Set nom
  *
  * @param string $nom
  *
  * @return Enseignant
  */
  public function setNom($nom)
  {
    $this->nom = $nom;

    return $this;
  }

  /**
  * Get nom
  *
  * @return string
  */
  public function getNom()
  {
    return $this->nom;
  }

  /**
  * Set prenom
  *
  * @param string $prenom
  *
  * @return Enseignant
  */
  public function setPrenom($prenom)
  {
    $this->prenom = $prenom;

    return $this;
  }

  /**
  * Get prenom
  *
  * @return string
  */
  public function getPrenom()
  {
    return $this->prenom;
  }

  //--------------------------------------------------------------------------

    /**
     * Add licence
     *
     * @param \PLU\CoreBundle\Entity\Licence $licence
     *
     * @return Enseignant
     */
    public function addLicences(\PLU\CoreBundle\Entity\Licence $licences)
    {
        $this->licences[] = $licences;

        return $this;
    }

    /**
     * Remove licence
     *
     * @param \PLU\CoreBundle\Entity\Licence $licence
     */
    public function removeLicences(\PLU\CoreBundle\Entity\Licence $licences)
    {
        $this->licences->removeElement($licences);
    }

    /**
     * Get licences
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLicences()
    {
        return $this->licences;
    }

    public function getLicencesArray() {
      foreach ($this->getLicences() as $key => $value) {
        $licencesArray[$value->getId()] = $value->getId();
      }
      return $licencesArray;
    }

    /**
     * Add licence
     *
     * @param \PLU\CoreBundle\Entity\Licence $licence
     *
     * @return Enseignant
     */
    public function addLicence(\PLU\CoreBundle\Entity\Licence $licence)
    {
        $this->licences[] = $licence;

        return $this;
    }

    /**
     * Remove licence
     *
     * @param \PLU\CoreBundle\Entity\Licence $licence
     */
    public function removeLicence(\PLU\CoreBundle\Entity\Licence $licence)
    {
        $this->licences->removeElement($licence);
    }
}
