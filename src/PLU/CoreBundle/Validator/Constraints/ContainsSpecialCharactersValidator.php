<?php

namespace PLU\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ContainsSpecialCharactersValidator extends ConstraintValidator
{
    public function validate($folder, Constraint $constraint)
    {	
    	$caracteresInterdits = array('/','\\','.',':','<','>','?','*','"','|');
        foreach ($caracteresInterdits as $key => $value) {
	        if (strpos($folder, $value) !== false) {
	            $this->context->buildViolation($constraint->message)
	                ->setParameter('{{ string }}', $folder)
	                ->addViolation();
                break;
	        }
    	}	
	}
}
