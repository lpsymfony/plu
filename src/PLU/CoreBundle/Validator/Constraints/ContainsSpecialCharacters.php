<?php

namespace  PLU\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsSpecialCharacters extends Constraint
{
    public $message = 'Le nom du dossier ne peut pas contenir les caractères suivants : / \ . : < > ? * " |';
}