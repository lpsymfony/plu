<?php

namespace PLU\CoreBundle\Form\Administrateur;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\CallbackValidator;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\NotBlank;


class AdministrateurAddType extends AbstractType{

  public function buildForm(FormBuilderInterface $builder, array $options){
    $builder
      ->add('password', PasswordType::class)
      ->add(
        'checkPassword', PasswordType::class,
        array(
          'constraints' => array(
            new NotBlank(
              array('message' => "Ce champ est obligatoire" )
            )
          )
        )
      )
    ;

    $builder->addEventListener(
      FormEvents::POST_SUBMIT,
      function ($event) {
        $administrateur = $event->getData();
        $form = $event->getForm();

        if (null === $administrateur) {
          return;
        }
        if ($administrateur->getPassword() != $administrateur->getCheckPassword()) {
          $form->get('password')->addError(new FormError('Les mots de passe ne sont pas identiques.'));
        }
      }
    );
  }




  public function setDefaultOptions(OptionResolverInterface $resolver){
    $resolver->setDefaults(
      array(
        'data_class' => 'PLU\CoreBundle\Entity\Administrateur'
      )
    );
  }

  public function getParent(){
    return AdministrateurType::class;
  }
}
