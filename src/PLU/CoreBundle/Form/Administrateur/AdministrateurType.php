<?php

namespace PLU\CoreBundle\Form\Administrateur;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use PLU\CoreBundle\Entity\Administrateur;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdministrateurType extends AbstractType
{
  /**
  * {@inheritdoc}
  */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('nom', TextType::class,
        array(
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('prenom', TextType::class,
        array(
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('username',     EmailType::class,
        array(
          'invalid_message' => 'Cette valeur n\'est pas valide.',
          'constraints' => array(
            new NotBlank(
              array(
                'message' => "Ce champ est obligatoire"
              )
            )
          )
        )
      )
      ->add('enregistrer',      SubmitType::class)
    ;
  }

  /**
  * {@inheritdoc}
  */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(
      array(
        'data_class' => 'PLU\CoreBundle\Entity\Administrateur'
      )
    );
  }

  /**
  * {@inheritdoc}
  */
  public function getBlockPrefix()
  {
    return 'plu_corebundle_administrateur';
  }
}
