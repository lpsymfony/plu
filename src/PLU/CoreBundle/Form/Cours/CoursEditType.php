<?php

namespace PLU\CoreBundle\Form\Cours;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use PLU\CoreBundle\Entity\Cours;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class CoursEditType extends AbstractType{

  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options) {

    $cours = new Cours();
    $folderList = $cours->getListFolder("cours");
    unset($folderList[array_search('', $folderList, true)]);

    $builder
      ->add('chemin', ChoiceType::class,
        array(
          'choices' => $folderList,
          'expanded' => false,
          'multiple' => false,
          'label' => false,
          'invalid_message' => 'Ce chemin n\'est pas valide.',
          'constraints' => array(
            new Assert\Choice(array_values($folderList)),
          )
        )
      )
      ->add('description',  CKEditorType::class)
      ->add('file',     FileType::class,
        array(
          'required' => false
        )
      )
      ->add('licences', EntityType::class,
        array(
          'class' => 'PLUCoreBundle:Licence',
          'choice_label' => 'nom',
          'multiple' => true,
          'expanded' => true,
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('visible', CheckboxType::class,
        array(
          'required' => false,
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('enregistrer',  SubmitType::class)
    ;
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver){
    $resolver->setDefaults(array(
      'data_class' => 'PLU\CoreBundle\Entity\Cours'
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockPrefix(){
    return 'plu_corebundle_cours';
  }


}
