<?php

namespace PLU\CoreBundle\Form\Cours;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;
use PLU\CoreBundle\Validator\Constraints as CustomAssert;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use PLU\CoreBundle\Entity\Cours;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class CoursCreateFolderType extends AbstractType{

  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options) {

    $cours = new Cours();
    $folderList = $cours->getListFolder("cours");
    if (!$folderList) {
      $folderList = array('/' => "");
    }

    $builder
      ->add('chemin', ChoiceType::class,
        array(
          'choices' => $folderList,
          'expanded' => false,
          'multiple' => false,
          'label' => false,
          'required' => false,
          'invalid_message' => 'Ce chemin n\'est pas valide.',
          'constraints' => array(
            new Assert\Choice(array_values($folderList)),
          )
        )
      )
      ->add('folder', TextType::class, array (
          'constraints' => array(
            new CustomAssert\ContainsSpecialCharacters(),
            new Assert\NotBlank(array('message' => "Ce champ est obligatoire.")),
          )
        )
      )
      ->add('enregistrer',  SubmitType::class)
    ;
  }
}
