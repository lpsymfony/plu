<?php

namespace PLU\CoreBundle\Form\Cours;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use PLU\CoreBundle\Validator\Constraints as CustomAssert;
use Symfony\Component\Validator\Constraints as Assert;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class CoursEditFolderType extends AbstractType{

  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options) {

    $builder
      ->add('path', TextType::class, array(
      		'constraints' => array(
      			new CustomAssert\ContainsSpecialCharacters(),
      			new Assert\NotBlank(array('message' => "Ce champ est obligatoire.")),
  			  ) 
      	)
      )
      ->add('enregistrer',  SubmitType::class)
    ;
  }
}
