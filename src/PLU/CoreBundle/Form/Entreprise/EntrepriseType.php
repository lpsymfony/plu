<?php

namespace PLU\CoreBundle\Form\Entreprise;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use PLU\CoreBundle\Entity\Entreprise;

class EntrepriseType extends AbstractType
{
  /**
  * {@inheritdoc}
  */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('nom',      TextType::class,
        array(
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('username',     EmailType::class,
        array(
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('addr',     TextType::class,
        array(
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('enregistrer',      SubmitType::class)
    ;
  }

  /**
  * {@inheritdoc}
  */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'PLU\CoreBundle\Entity\Entreprise',
    ));
  }

  /**
  * {@inheritdoc}
  */
  public function getBlockPrefix()
  {
    return 'plu_corebundle_entreprise';
  }
}
