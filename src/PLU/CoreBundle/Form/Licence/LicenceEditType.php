<?php

namespace PLU\CoreBundle\Form\Licence;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvents;

use PLU\CoreBundle\Entity\Enseignant;
use PLU\CoreBundle\Repository\EnseignantRepository;

class LicenceEditType extends AbstractType
{
  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options) {

    $builder
      ->addEventListener(
        FormEvents::POST_SET_DATA,
        function ($event) {
          $licence = $event->getData();
          $event->getForm()->add(
            'responsable',
            EntityType::class,
            array(
              'class' => 'PLUCoreBundle:Enseignant',
              'choice_label' => 'nom',
              'choices' =>$licence->getEnseignants(),
              'multiple'      => false,
              'expanded'      => false,
              'required'      => false
            )
          );
        }
      )
    ;
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver){
    $resolver->setDefaults(array(
      'data_class' => 'PLU\CoreBundle\Entity\Licence'
    ));
  }

  public function getParent(){
    return LicenceType::class;
  }

}
