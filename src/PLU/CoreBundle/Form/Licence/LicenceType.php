<?php

namespace PLU\CoreBundle\Form\Licence;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class LicenceType extends AbstractType
{
  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options) {

    $builder
      ->add('nom',   TextType::class, array (
            'invalid_message' => 'Cette valeur n\'est pas valide.'))
      ->add('infoGeneral',  CKEditorType::class, array (
            'invalid_message' => 'Cette valeur n\'est pas valide.'))
      ->add('enregistrer',  SubmitType::class)
    ;
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver){
    $resolver->setDefaults(array(
      'data_class' => 'PLU\CoreBundle\Entity\Licence'
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockPrefix(){
    return 'plu_corebundle_licence';
  }


}
