<?php

namespace PLU\CoreBundle\Form\Etudiant;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use PLU\CoreBundle\Entity\Etudiant;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EtudiantRoleType extends AbstractType
{
  /**
  * {@inheritdoc}
  */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {

    $form = $builder->getData();

    $builder
      ->add('licence', EntityType::class,
        array(
          'class' => 'PLUCoreBundle:Licence',
          'choice_label'  => 'nom', 
          'multiple'      => false,
          'expanded'      => false,
          'required'      => false,
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('roles', ChoiceType::class,
        array(
          'mapped' => false,
          'choices' => array(
            'Futur étudiant' => 'ROLE_ETUDIANT_FUTUR',
            'Etudiant actuel' => 'ROLE_ETUDIANT_ACTUEL',
            'Ancien étudiant' => 'ROLE_ETUDIANT_ANCIEN',
          ),
          'expanded' => false,
          'multiple' => false,
          'preferred_choices' => array(
            $form->getRoles()[0]
          ),
          'constraints' => array(
            new Assert\Choice(
              array(
                'ROLE_ETUDIANT_FUTUR', 'ROLE_ETUDIANT_ACTUEL', 'ROLE_ETUDIANT_ANCIEN'
              )
            ),
            new Assert\NotBlank(
              array(
                'message' => "Ce champ est obligatoire."
              )
            ),
          ),
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('enregistrer', SubmitType::class)
    ;
  }

  /**
  * {@inheritdoc}
  */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'PLU\CoreBundle\Entity\Etudiant',
    ));
  }

  /**
  * {@inheritdoc}
  */
  public function getBlockPrefix()
  {
    return 'plu_corebundle_etudiant';
  }
}
