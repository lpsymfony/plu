<?php

namespace PLU\CoreBundle\Form\Etudiant;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use PLU\CoreBundle\Entity\Etudiant;

class EtudiantType extends AbstractType
{
  /**
  * {@inheritdoc}
  */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $etudiant = new Etudiant();
    $builder
      ->add('nom', TextType::class,
        array(
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('prenom', TextType::class,
        array(
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('dateNaissance', DateType::class,
        array(
          'years' => range(date('Y') - 15, date('Y') - 100),
          'invalid_message' => 'Cette valeur n\'est pas valide.',
          'format' => 'dd MM yyyy',
          'widget' => 'choice'
        )
      )
      ->add('sexe', ChoiceType::class,
        array(
          'choices' => array('Homme' => 'm', 'Femme' => 'f'),
          'expanded' => false,
          'multiple' => false,
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('telephone', TextType::class,
        array(
          'required' => false,
          'invalid_message' => 'Cette valeur n\'est pas valide.',
          'attr' => array('placeholder' => 'Exemple : 0612345678')
        )
      )
      ->add('typeBac', TextType::class,
        array(
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('username', EmailType::class,
        array(
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('emailInstit', EmailType::class,
        array(
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('anneeBac', ChoiceType::class,
        array(
          'choices' => $etudiant->yearsRange(),
          'expanded' => false,
          'multiple' => false,
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('ville', TextType::class,
        array(
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('rue', TextType::class,
        array(
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('codePostal', TextType::class,
        array(
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('typeBacSup', TextType::class,
        array(
          'required' => false,
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('anneeBacSup', ChoiceType::class,
        array(
          'choices' => $etudiant->yearsRange(),
          'expanded' => false,
          'multiple' => false,
          'required' => false,
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('enregistrer', SubmitType::class)
    ;
  }

  /**
  * {@inheritdoc}
  */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(
      array(
        'data_class' => 'PLU\CoreBundle\Entity\Etudiant',
      )
    );
  }

  /**
  * {@inheritdoc}
  */
  public function getBlockPrefix()
  {
    return 'plu_corebundle_etudiant';
  }
}
