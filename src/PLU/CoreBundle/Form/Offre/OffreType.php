<?php

namespace PLU\CoreBundle\Form\Offre;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

// use PLU\CoreBundle\Entity\Offre;
// use PLU\CoreBundle\Entity\Categorie;
// use PLU\CoreBundle\Entity\Entreprise;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class OffreType extends AbstractType{

  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options) {

    $builder
      ->add('intitule',   TextType::class)
      // ->add('entreprise', EntityType::class,
      //   array(
      //     'class' => 'PLUCoreBundle:Entreprise',
      //     'choice_label'  => 'nom',
      //     'multiple'      => false,
      //     'expanded'      => false,
      //     'required'      => false
      //   )
      // )
      ->add('categorie', EntityType::class,
        array(
          'class' => 'PLUCoreBundle:Categorie',
          'choice_label'  => 'nom',
          'multiple'      => false,
          'expanded'      => false,
          'required'      => false
        )
      )
      ->add('description',  CKEditorType::class)
      ->add('file',     FileType::class,
        array(
          'required' => false
        )
      )
      ->add('addr',         TextType::class)
      ->add('publie',       CheckboxType::class,
        array(
          'required' => false
        )
      )
      ->add('enregistrer',  SubmitType::class)
    ;
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver){
    $resolver->setDefaults(array(
      'data_class' => 'PLU\CoreBundle\Entity\Offre'
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockPrefix(){
    return 'plu_corebundle_offre';
  }


}
