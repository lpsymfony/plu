<?php

namespace PLU\CoreBundle\Form\Offre;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\CallbackValidator;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\NotBlank;


class AdministrateurAddOffreType extends AbstractType{

  public function buildForm(FormBuilderInterface $builder, array $options){
    $builder
      ->add('entreprise', EntityType::class,
        array(
          'class' => 'PLUCoreBundle:Entreprise',
          'choice_label'  => 'nom',
          'multiple'      => false,
          'expanded'      => false,
          'required'      => false
        )
      )
    ;
  }

  public function setDefaultOptions(OptionResolverInterface $resolver){
    $resolver->setDefaults(
      array(
        'data_class' => 'PLU\CoreBundle\Entity\Offre'
      )
    );
  }

  public function getParent(){
    return AdministrateurOffreType::class;
  }
}
