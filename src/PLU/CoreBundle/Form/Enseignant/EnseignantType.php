<?php

namespace PLU\CoreBundle\Form\Enseignant;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class EnseignantType extends AbstractType
{
  /**
  * {@inheritdoc}
  */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('nom', TextType::class)
      ->add('prenom', TextType::class)
      ->add('username', EmailType::class)
      ->add('licences', EntityType::class,
        array(
          'class' => 'PLUCoreBundle:Licence',
          'choice_label' => 'nom',
          'multiple' => true,
          'expanded' => true
        )
      )
      ->add('enregistrer', SubmitType::class)
    ;
  }

  /**
  * {@inheritdoc}
  */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'PLU\CoreBundle\Entity\Enseignant'
    ));
  }

  /**
  * {@inheritdoc}
  */
  public function getBlockPrefix()
  {
    return 'plu_corebundle_enseignant';
  }


}
