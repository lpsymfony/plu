<?php

namespace PLU\CoreBundle\Form\Enseignant;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class EnseignantEditType extends AbstractType{

  public function buildForm(FormBuilderInterface $builder, array $options){

  }

  public function getParent(){
    return EnseignantType::class;
  }
}
