<?php

namespace PLU\CoreBundle\Form\Utilisateur;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use PLU\CoreBundle\Entity\Utilisateur;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Constraints\NotBlank;

class UtilisateurChangePasswordType extends AbstractType
{
  /**
  * {@inheritdoc}
  */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('password', PasswordType::class,
        array(
          'invalid_message' => 'Le mot de passe doit correspondre.'
        )
      )
      ->add('checkPassword',     PasswordType::class,
        array(
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('enregistrer',      SubmitType::class)
    ;

    $builder->addEventListener(
      FormEvents::POST_SUBMIT,
      function ($event) {
        $utilisateur = $event->getData();
        $form = $event->getForm();

        if (null === $utilisateur) {
          return;
        }
        if ($form["password"]->getData() != $form["checkPassword"]->getData()) {
          $form->get('password')->addError(new FormError('Les mots de passe ne sont pas identiques !'));
        }
      }
    );
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(
      array(
        'data_class' => 'PLU\CoreBundle\Entity\Utilisateur',
      )
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockPrefix()
  {
    return 'plu_corebundle_utilisateur';
  }
}
