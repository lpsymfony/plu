<?php

namespace PLU\CoreBundle\Form\Page;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class PageType extends AbstractType
{

  /*protected $allRoles;

  public function __construct($allRoles)
  {
      $this->allRoles = $allRoles;
  }*/

  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {

    $allRoles = $builder->getData()->getAllRoles();
    $roles = $builder->getData()->getRoles();

    $builder
      ->add('intitule', TextType::class)
      ->add('description', CKEditorType::class)
      // ->add('visible', CheckboxType::class,
      //   array(
      //     'required' => false
      //   )
      // )
      ->add('allRoles', ChoiceType::class,
        array(
          'choices' => $allRoles,
          'expanded' => true,
          'multiple' => true,
          'data' => $roles,
          'invalid_message' => 'Cette valeur n\'est pas valide.'
        )
      )
      ->add('enregistrer',  SubmitType::class)
    ;
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'PLU\CoreBundle\Entity\Page'
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockPrefix()
  {
    return 'plu_corebundle_page';
  }


}
