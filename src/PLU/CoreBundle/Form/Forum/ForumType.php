<?php

namespace PLU\CoreBundle\Form\Forum;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class ForumType extends AbstractType
{
  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options) {

    $builder
      ->add('titre',   TextType::class, array (
            'invalid_message' => 'Cette valeur n\'est pas valide.'))
      ->add('description',  CKEditorType::class)
      ->add('sujet', EntityType::class,
        array(
          'class' => 'PLUCoreBundle:Sujet',
          'choice_label'  => 'nom',
          'multiple'      => false,
          'expanded'      => false,
          'required'      => false
        )
      )
      ->add('enregistrer',  SubmitType::class)
    ;
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver){
    $resolver->setDefaults(array(
      'data_class' => 'PLU\CoreBundle\Entity\Forum'
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockPrefix(){
    return 'plu_corebundle_forum';
  }

}
