<?php

namespace PLU\CoreBundle\Form\Agenda;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Validator\Constraints\NotBlank;


class AgendaType extends AbstractType
{
  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options) {

    $builder
      ->add('date', DateType::class,
        array(
          'invalid_message' => 'Cette valeur n\'est pas valide.',
          'widget' => 'single_text',
          'data' => new \DateTime("now"),
          'attr' => ['class' => 'js-datepicker'],
        )
      )
      ->add('description',  TextType::class, array (
            'invalid_message' => 'Cette valeur n\'est pas valide.'))
      ->add('licences', EntityType::class,
        array(
          'class' => 'PLUCoreBundle:Licence',
          'choice_label'  => 'nom',
          'multiple'      => true,
          'expanded'      => true,
          'required'      => true,
          'constraints' => array(
            new NotBlank(
              array(
                'message' => "Ce champ est obligatoire."
              )
            )
          )
        )
      )
      ->add('enregistrer',  SubmitType::class)
    ;
  }

  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver){
    $resolver->setDefaults(array(
      'data_class' => 'PLU\CoreBundle\Entity\Agenda'
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockPrefix(){
    return 'plu_corebundle_agenda';
  }


}
