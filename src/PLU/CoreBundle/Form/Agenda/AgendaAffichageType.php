<?php

namespace PLU\CoreBundle\Form\Agenda;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints as Assert;


class AgendaAffichageType extends AbstractType
{
  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options) {

    $date = array();
    for ($i=-4; $i < 4; $i++) { 
      $date[date('m/Y', strtotime('+'.$i.' month'))] = date('Y-m', strtotime('+'.$i.' month'));
    }

    $builder
      ->add('licence', EntityType::class,
        array(
          'class' => 'PLUCoreBundle:Licence',
          'choice_label' => 'nom',
          'expanded' => false,
          'multiple' => false,
          'label' => false,
          'invalid_message' => 'Cette licence n\'est pas valide.'
        )
      )
      ->add('date', ChoiceType::class,
        array(
          'choices' => $date,
          'expanded' => false,
          'multiple' => false,
          'label' => false,
          'invalid_message' => 'Cette date n\'est pas valide.',
          'constraints' => array(
            new Assert\Choice(array_values($date)),
            new Assert\NotBlank(
              array(
                'message' => "Le champ date est obligatoire."
              )
            ),
          ),
        )
      )
    ;
  }

/*   public function getBlockPrefix(){
    return 'plu_corebundle_agenda';
  }
*/

}
