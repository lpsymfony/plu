<?php

namespace PLU\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use PLU\CoreBundle\Entity\Enseignant;
use PLU\CoreBundle\Form\Enseignant\EnseignantType;
use PLU\CoreBundle\Form\Enseignant\EnseignantAddType;
use PLU\CoreBundle\Form\EnseignantEditType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class EnseignantController extends Controller
{
  public function indexAction(Request $request){
    if (
      // false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
    ) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }
    $listEnseignant = $this
    ->getDoctrine()
    ->getManager()
    ->getRepository('PLUCoreBundle:Enseignant')
    ->findAll()
    ;

    return $this->render('PLUCoreBundle:Enseignant:index.html.twig', array(
      'listEnseignant' => $listEnseignant)
    );
  }

  public function addAction(Request $request){

    //Vérifie que l'enseignant ne soit pas déjà connecté et qu'il ne soit pas un administrateur
    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
    ) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $enseignant = new Enseignant();
    $form = $this->get('form.factory')->create(EnseignantAddType::class, $enseignant);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em = $this->getDoctrine()->getManager();

      $salt = md5(uniqid());
      $encoded = $this->get('security.encoder_factory')->getEncoder($enseignant)->encodePassword($enseignant->getPassword(), $salt);

      //Ajout de l'enseignant dans la table générique enseignant
      $enseignant->setPassword($encoded);
      $enseignant->setSalt($salt);

      $em->persist($enseignant);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Inscription bien enregistrée.');

      if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
        return $this->redirectToRoute('plu_core_licence_homepage');
      } else {
        return $this->redirectToRoute('plu_core_licence_homepage');
      }
    }
    return $this->render(
      'PLUCoreBundle:Enseignant:add.html.twig',
      array(
        'form' => $form->createView()
      )
    );
  }

  public function editAction(Request $request, $id){
    //Vérifie que l'enseignant est connecté
    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
      $request->getSession()->getFlashBag()->add('danger', "Veuillez vous connecter pour accéder à cette page.");
      return $this->redirectToRoute('login');
    }

    $user  = $this->getDoctrine()->getManager()->getRepository('PLUCoreBundle:Utilisateur')->findOneByUsername($id);

    //Vérifie que l'enseignant est bien celui qui essaie de modifier son profil ou bien que l'utilisateur est un administrateur
    if (
      $this->getUser()->getUsername()!==$user->getUsername()
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
    ) {
      $request->getSession()->getFlashBag()->add('danger', 'Vous n\'avez pas le droit d\'accéder à ce profil.');
      return $this->redirectToRoute('plu_core_licence_homepage');
    }
    $enseignant  = $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('PLUCoreBundle:Enseignant')
      ->findOneByUsername($id)
    ;

    if (null === $enseignant) {
      $request->getSession()->getFlashBag()->add('danger', "Il n'existe pas d'enseignant avec l'e-mail ".$id.".");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $form   = $this->get('form.factory')->create(EnseignantType::class, $enseignant);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em = $this->getDoctrine()->getManager();

      $em->persist($enseignant);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Profil bien modifié.');
      if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
        return $this->redirectToRoute('plu_core_enseignant_homepage');
      } else {
        return $this->redirectToRoute('plu_core_licence_homepage');
      }
    }
    return $this->render('PLUCoreBundle:Enseignant:edit.html.twig', array(
      'form' => $form->createView(),
    ));
  }

  public function deleteAction(Request $request, $id){

    //Vérifie que l'utilisateur soit un administrateur
    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $em = $this->getDoctrine()->getManager();
    $enseignant = $em->getRepository('PLUCoreBundle:Utilisateur')->findOneByUsername($id);

    if (null === $enseignant) {
      $request->getSession()->getFlashBag()->add('danger', "Il n'existe pas d'enseignant avec l'e-mail ".$id.".");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $form = $this->get('form.factory')->create();

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em->remove($enseignant);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', "L'enseignant a bien été supprimé.");

      return $this->redirectToRoute('plu_core_enseignant_homepage');
    }

    return $this->render('PLUCoreBundle:Enseignant:delete.html.twig', array(
      'enseignant' => $enseignant,
      'form' => $form->createView(),
    ));
  }
}
