<?php

namespace PLU\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use PLU\CoreBundle\Entity\Administrateur;
use PLU\CoreBundle\Form\Administrateur\AdministrateurType;
use PLU\CoreBundle\Form\Administrateur\AdministrateurAddType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Finder\Exception\AccessDeniedException;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AdministrateurController extends Controller {

	public function indexAction(Request $request){
		if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
			$request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}
		$listAdmin = $this->getDoctrine()->getManager()->getRepository('PLUCoreBundle:Administrateur')->findAll();
		return $this->render('PLUCoreBundle:Administrateur:index.html.twig', array('listAdmin' => $listAdmin));
	}

	public function addAction(Request $request){
		if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
			$request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		$admin = new Administrateur();
		$form = $this->get('form.factory')->create(AdministrateurAddType::class, $admin);

		if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
			$salt = md5(uniqid());
			$encoded = $this->get('security.encoder_factory')->getEncoder($admin)->encodePassword($admin->getPassword(), $salt);

			$admin->setUsername($admin->getUsername());
			$admin->setPassword($encoded);
			$admin->setSalt($salt);

			$em = $this->getDoctrine()->getManager();
			$em->persist($admin);
			$em->flush();

			$request->getSession()->getFlashBag()->add('notice', 'Administrateur bien enregistrée.');
			return $this->redirectToRoute('plu_core_administrateur_homepage');
		}
		return $this->render('PLUCoreBundle:Administrateur:add.html.twig',array('form' => $form->createView()));
	}

	public function editAction(Request $request, $id){
		if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
			$request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas le droit d'accéder à ce profil.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}
		
		$em = $this->getDoctrine()->getManager();
		$admin  = $em->getRepository('PLUCoreBundle:Administrateur')->findOneByUsername($id);

		if (null === $admin) {
			$request->getSession()->getFlashBag()->add('danger', "Il n'existe pas d'administrateur avec l'e-mail \"$id\".");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		$form   = $this->get('form.factory')->create(AdministrateurType::class, $admin);

		if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
			$em->persist($admin);
			$em->flush();

			$request->getSession()->getFlashBag()->add('notice', 'Profil bien modifié.');
			return $this->redirectToRoute('plu_core_administrateur_homepage');
		}
		return $this->render('PLUCoreBundle:Administrateur:edit.html.twig', array('form' => $form->createView()));
	}

	public function deleteAction(Request $request, $id){

		if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
			$request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		$em = $this->getDoctrine()->getManager();
		$admin = $em->getRepository('PLUCoreBundle:Utilisateur')->findOneByUsername($id);

		if (null === $admin) {
			$request->getSession()->getFlashBag()->add('danger', "Il n'existe pas d'administrateur avec l'e-mail \"$id\".");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		$form = $this->get('form.factory')->create();

		if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
			$adminUsername = $admin->getUsername();
			$currentUsername = $this->getUser()->getUsername();
			$em->remove($admin);
			$em->flush();

			if ($currentUsername === $adminUsername ) {
				$this->get("security.token_storage")->setToken(null);
				$this->get('session')->invalidate();
				$request->getSession()->getFlashBag()->add('notice', "Votre compte administrateur a bien été supprimé.");
				return $this->redirectToRoute('logout');
			} else {
				$request->getSession()->getFlashBag()->add('notice', "L'administrateur a bien été supprimée.");
				return $this->redirectToRoute('plu_core_administrateur_homepage');
			}
		}

		return $this->render('PLUCoreBundle:Administrateur:delete.html.twig',
		array('admin' => $admin, 'form' => $form->createView(),));
	}
}
