<?php

namespace PLU\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use PLU\CoreBundle\Entity\Etudiant;
use PLU\CoreBundle\Entity\Entreprise;
use PLU\CoreBundle\Form\Etudiant\EtudiantType;
use PLU\CoreBundle\Form\Etudiant\EtudiantAddType;
use PLU\CoreBundle\Form\Etudiant\EtudiantRoleType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Finder\Exception\AccessDeniedException;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EtudiantController extends Controller {

	public function indexAction(Request $request){
		if (false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
		&& false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {

			$request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}
		$listEtudiant = $this->getDoctrine()->getManager()->getRepository('PLUCoreBundle:Etudiant')->findAll();
		return $this->render('PLUCoreBundle:Etudiant:index.html.twig', array('listEtudiant' => $listEtudiant));
	}

	public function addAction(Request $request){
		//Vérifie que l'utilisateur ne soit pas déjà connecté et qu'il ne soit pas un administrateur
		if (
			false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
			&& false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
		) {
			$request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		$etudiant = new Etudiant();
		$form   = $this->get('form.factory')->create(EtudiantAddType::class, $etudiant);

		if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
			$em = $this->getDoctrine()->getManager();
			if ($etudiant->getPassword()===$etudiant->getCheckPassword()) {

				$salt = md5(uniqid());
				$encoded = $this->get('security.encoder_factory')->getEncoder($etudiant)->encodePassword($etudiant->getPassword(), $salt);

				//Ajout de l'étudiant dans la table générique utilisateur
				$etudiant->setPassword($encoded);
				$etudiant->setSalt($salt);

				$em->persist($etudiant);
				$em->flush();

				$request->getSession()->getFlashBag()->add('notice', 'Inscription bien enregistrée.');

				//Redirige l'utilisateur sur la liste des étudiants si celui-ci est administrateur, sinon sur la liste des licences
				if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
					return $this->redirectToRoute('plu_core_etudiant_homepage');
				} else {
					return $this->redirectToRoute('plu_core_licence_homepage');
				}
			}
			$request->getSession()->getFlashBag()->add('danger', 'Les mots de passe ne sont pas identiques');
		}
		return $this->render('PLUCoreBundle:Etudiant:add.html.twig',array('form' => $form->createView()));
	}

	public function editAction(Request $request, $id){
		//Vérifie que l'utilisatateur est connecté
		if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
			$request->getSession()->getFlashBag()->add('danger', "Veuillez vous connecter pour accéder à cette page.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		$em = $this->getDoctrine()->getManager();
		$etudiant  = $em->getRepository('PLUCoreBundle:Etudiant')->findOneByUsername($id);

		if (null === $etudiant) {
			$request->getSession()->getFlashBag()->add('danger', "Il n'existe pas d'étudiant avec l'e-mail \"$id\".");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		//Vérifie que l'utilisateur qui essaie de modifier son profil ou bien que l'utilisateur est un administrateur
		if (
			$this->getUser()->getUsername()!==$etudiant->getUsername()
			&& false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
			&& false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
		) {
			$request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas le droit d'accéder à ce profil.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		$form   = $this->get('form.factory')->create(EtudiantType::class, $etudiant);

		if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
			$em->persist($etudiant);
			$em->flush();

			$request->getSession()->getFlashBag()->add('notice', 'Profil bien modifié.');

			//Redirige l'utilisateur sur la liste des étudiants si celui-ci est administrateur, sinon sur la liste des licences
			if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
				return $this->redirectToRoute('plu_core_etudiant_homepage');
			} else {
				return $this->redirectToRoute('plu_core_licence_homepage');
			}
		}
		return $this->render('PLUCoreBundle:Etudiant:edit.html.twig', array('form' => $form->createView()));
	}

	public function deleteAction(Request $request, $id){

		//Vérifie que l'utilisateur soit un administrateur
		if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
			$request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		$em = $this->getDoctrine()->getManager();
		$etudiant = $em->getRepository('PLUCoreBundle:Utilisateur')->findOneByUsername($id);

		if (null === $etudiant) {
			$request->getSession()->getFlashBag()->add('danger', "Il n'existe pas d'étudiant avec l'e-mail \"$id\".");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		$form = $this->get('form.factory')->create();

		if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
			$em->remove($etudiant);
			$em->flush();
			$request->getSession()->getFlashBag()->add('notice', "L'étudiant a bien été supprimé.");
			return $this->redirectToRoute('plu_core_etudiant_homepage');
		}

		return $this->render('PLUCoreBundle:Etudiant:delete.html.twig',
		array('etudiant' => $etudiant, 'form' => $form->createView()));
	}

	public function editRoleAction(Request $request, $id){
		//Vérifie que l'utilisatateur soit connecté
		if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
			$request->getSession()->getFlashBag()->add('danger', "Veuillez vous connecter pour accéder à cette page.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		$user  = $this->getDoctrine()->getManager()->getRepository('PLUCoreBundle:Utilisateur')->findOneByUsername($id);

		if (null === $user) {
			$request->getSession()->getFlashBag()->add('danger', "Il n'existe pas d'étudiant avec l'e-mail \"$id\".");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		//Vérifie que l'utilisateur soit un administrateur ou un responsable licence
		if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
		&& false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')) {
			$request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas le droit d'accéder à ce profil.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		$form = $this->get('form.factory')->create(EtudiantRoleType::class, $user);

		if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
			$em = $this->getDoctrine()->getManager();


			if ($form["roles"]->getData() === "ROLE_ETUDIANT_ACTUEL") {
				$user->setRoles(array('ROLE_ETUDIANT_ACTUEL'));
			} elseif ($form["roles"]->getData() === "ROLE_ETUDIANT_ANCIEN") {
				$user->setRoles(array('ROLE_ETUDIANT_ANCIEN'));
			} elseif ($form["roles"]->getData() === "ROLE_ETUDIANT_FUTUR") {
				$user->setRoles(array('ROLE_ETUDIANT_FUTUR'));
			}

			$em->persist($user);
			$em->flush();

			$request->getSession()->getFlashBag()->add('notice', 'Status bien modifié.');
			return $this->redirectToRoute('plu_core_etudiant_homepage');

		}

		return $this->render('PLUCoreBundle:Etudiant:editRole.html.twig', array('form' => $form->createView()));
	}

	/*public function testAction(){
	$em = $this->getDoctrine()->getManager();

	$etudiant = new Etudiant();
	$etudiant->setNom("Poirot");
	$etudiant->setPrenom("Florian");
	$etudiant->setDateNaissance(new \Datetime());
	$etudiant->setSexe("H");
	$etudiant->setTypeBac("Techno");
	$etudiant->setEmailPerso("florian.poirot@etu.umontpellier.fr");
	$etudiant->setEmailInstit("florian.poirot@etu.umontpellier.fr");
	$etudiant->setRue("13 rue marcel proust");
	$etudiant->setVille("Lezignan-Corbieres");
	$etudiant->setCodePostal("11200");
	$etudiant->setMotDePasse("florian.poirot");

	$etudiant->setUsername("florian.poirot");
	$etudiant->setPassword("florian.poirot");
	$etudiant->setSalt('');
	$etudiant->setRoles(array('ROLE_ETUDIANT_ACTUEL'));

	//---------------------------------------------------

	$entreprise = new Entreprise();
	$entreprise->setNom("CGI");
	$entreprise->setAddr("400 Avenue Marcel Dassault, 34170 Castelnau-le-Lez");

	$entreprise->setUsername("cgi");
	$entreprise->setPassword("cgi");
	$entreprise->setSalt('');
	$entreprise->setRoles(array('ROLE_ENTREPRISE'));

	$em->persist($entreprise);
	$em->persist($etudiant);
	$em->flush();

	return $this->redirectToRoute('plu_core_offre_homepage');
}*/
}
