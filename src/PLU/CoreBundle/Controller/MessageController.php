<?php

namespace PLU\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use PLU\CoreBundle\Entity\Message;
use PLU\CoreBundle\Form\Message\MessageType;

class MessageController extends Controller {

  public function addAction(Request $request, $id) {

    $parent = $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('PLUCoreBundle:Message')
      ->find($id)
    ;

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ANCIEN')
    ) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_forum_view', array('id' => $parent->getForum()->getId()));
    }

    if (null === $parent) {
      $request->getSession()->getFlashBag()->add('danger', 'Le message n\'existe pas.');

      return $this->redirectToRoute('plu_core_forum_homepage');
    }

    $message = new Message();
    $message->setForum($parent->getForum());
    $message->setUtilisateur($this->getUser());
    $message->setMessage($parent);
    $form = $this->get('form.factory')->create(MessageType::class, $message);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($message);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Message bien enregistré.');

      return $this->redirectToRoute('plu_core_forum_view', array('id' => $message->getForum()->getId()));
    }

    return $this->render(
      'PLUCoreBundle:Message:add.html.twig',
      array(
        'form' => $form->createView()
      )
    );
  }

  public function editAction(Request $request, $id){

    $message = $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('PLUCoreBundle:Message')
      ->find($id)
    ;

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ANCIEN')
    ) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_forum_view', array('id' => $message->getForum()->getId()));
    }

    if (null === $message) {
      $request->getSession()->getFlashBag()->add('danger', 'Ce message n\'existe pas.');

      return $this->redirectToRoute('plu_core_forum_homepage');
    }

    if (
      $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      || $message->getUtilisateur()->getId() == $this->getUser()->getId()
    ) {
      $form = $this->get('form.factory')->create(MessageType::class, $message);
    } else {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_forum_view', array('id' => $message->getForum()->getId()));
    }

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($message);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Message bien enregistré.');

      return $this->redirectToRoute('plu_core_forum_view', array('id' => $message->getForum()->getId()));
    }
    return $this->render(
      'PLUCoreBundle:Message:edit.html.twig',
      array(
        'message' => $message,
        'form' => $form->createView()
      )
    );
	}

  public function deleteAction(Request $request, $id){
    
    $em = $this->getDoctrine()->getManager();
    $message = $em->getRepository('PLUCoreBundle:Message')->find($id);

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_forum_view', array('id' => $message->getForum()->getId()));
    }

    $idForum = $message->getForum()->getId();

    if (null === $message) {
      $request->getSession()->getFlashBag()->add('danger', 'Ce message n\'existe pas');

      return $this->redirectToRoute('plu_core_forum_homepage');
    }

    $form = $this->get('form.factory')->create();

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em->remove($message);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', "Le message a bien été supprimé.");

      return $this->redirectToRoute('plu_core_forum_view', array('id' => $idForum));
    }

    return $this->render('PLUCoreBundle:Message:delete.html.twig', array(
      'message' => $message,
      'form'   => $form->createView(),
    ));
	}

}

?>
