<?php

namespace PLU\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use PLU\CoreBundle\Entity\Utilisateur;
use PLU\CoreBundle\Form\Utilisateur\UtilisateurType;
use PLU\CoreBundle\Form\Utilisateur\UtilisateurChangePasswordType;

class UtilisateurController extends Controller{

  public function loginAction(Request $request){
    if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
      return $this->redirectToRoute('plu_core_homepage_licence');
    }

    // $authenticationUtils = $this->get('security.authentication_utils');
    //
    // return $this->render('PLUCoreBundle:Utilisateur:login.html.twig', array(
    //   'last_username' => $authenticationUtils->getLastUsername(),
    //   'error'         => $authenticationUtils->getLastAuthenticationError(),
    // ));
    return $this->redirectToRoute('plu_core_licence_homepage');
  }

  public function sendAction(Request $request){
    if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
      return $this->redirectToRoute('plu_core_licence_homepage');
    }
    $em = $this->getDoctrine()->getManager();

    if ($request->request->has('_username')) {
      $username = $request->request->get('_username');
      $user = $em->getRepository('PLUCoreBundle:Utilisateur')->findOneByUsername($username);

      if (null !== $user) {

        $keyreset = md5(uniqid());
        $message = \Swift_Message::newInstance()
        ->setSubject('Réinitialisation de votre mot de passe')
        ->setFrom('plu110700@gmail.com')
        ->setTo($username)
        ->setBody($this->renderView(
          'PLUCoreBundle:Utilisateur:link.html.twig',
          array(
            'keyreset' => $keyreset, 'username' => $username)
          ),
          'text/html'
        );

        # Send the message
        if ($this->get('mailer')->send($message)) {
          $request->getSession()->getFlashBag()->add('notice', "Les instructions à suivre ont été envoyées par email.");
        } else {
          $request->getSession()->getFlashBag()->add('danger', "Erreur lors de l'envoi du message !");
        }

        $user->setKeyreset($keyreset);
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('plu_core_licence_homepage');

      } else {
        $request->getSession()->getFlashBag()->add('danger', "L'email saisi n'existe pas.");
        return $this->redirectToRoute('plu_core_utilisateur_send');
      }
    }

    return $this->render('PLUCoreBundle:Utilisateur:send.html.twig');
  }

  public function resetAction(Request $request, $keyreset, $username){
    if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $em = $this->getDoctrine()->getManager();
    $user = $em->getRepository('PLUCoreBundle:Utilisateur')->findOneByUsername($username);
    if ($user && $keyreset===$user->getKeyreset()) {
      $form = $this->get('form.factory')->create(UtilisateurChangePasswordType::class, $user);
      if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
        $salt = md5(uniqid());
        $encoded = $this->get('security.encoder_factory')->getEncoder($user)->encodePassword($user->getPassword(), $salt);
        $user->setSalt($salt);
        $user->setPassword($encoded);
        $user->setKeyreset(null);
        $em->persist($user);
        $em->flush();
        $request->getSession()->getFlashBag()->add('notice', "Le mot de passe a bien été modifié.");
        return $this->redirectToRoute('plu_core_licence_homepage');
      }
    } else {
      throw new NotFoundHttpException("Il semblerait que ce lien ne soit pas valide !");
    }

    return $this->render('PLUCoreBundle:Utilisateur:reset.html.twig',
    array ('username' => $username, 'keyreset' => $keyreset, 'form' => $form->createView()));
  }

  public function changePasswordAction(Request $request, $id){
    //Vérifie que l'utilisatateur est connecté
    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
      $request->getSession()->getFlashBag()->add('danger', "Veuillez vous connecter pour accéder à cette page.");
      return $this->redirectToRoute('login');
    }

    $user  = $this->getDoctrine()->getManager()->getRepository('PLUCoreBundle:Utilisateur')->findOneByUsername($id);

    if (null === $user) {
      $request->getSession()->getFlashBag()->add('danger', "Il n'existe pas d'utilisateur avec l'e-mail ".$id.".");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    //Vérifie que l'utilisateur essaie de modifier son profil ou bien que l'utilisateur est un administrateur
    if ($this->getUser()->getUsername()!==$user->getUsername() && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
      $request->getSession()->getFlashBag()->add("danger", "Vous n'avez pas le droit d'accéder à ce profil.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $form = $this->get('form.factory')->create(UtilisateurType::class, $user);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em = $this->getDoctrine()->getManager();

      $salt = md5(uniqid());
      $encoded = $this->get('security.encoder_factory')->getEncoder($user)->encodePassword($form["password"]->getData(), $salt);

      $user->setPassword($encoded);
      $user->setSalt($salt);

      $em->persist($user);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Votre mot de passe a été modifié.');
      return $this->redirectToRoute('plu_core_licence_homepage');

    }
    return $this->render('PLUCoreBundle:Utilisateur:changePassword.html.twig', array(
      'form' => $form->createView(),
    ));
  }

}
