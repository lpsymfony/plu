<?php

namespace PLU\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use PLU\CoreBundle\Entity\Forum;
use PLU\CoreBundle\Entity\Message;
use PLU\CoreBundle\Form\Message\MessageType;
use PLU\CoreBundle\Form\Forum\ForumType;

class ForumController extends Controller {

  public function indexAction(Request $request){

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
    ){
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

		$sujets = $this->getDoctrine()->getManager()->getRepository('PLUCoreBundle:Sujet')->findAll();

    $authenticationUtils = $this->get('security.authentication_utils');
    return $this->render(
      'PLUCoreBundle:Forum:index.html.twig',
      array(
        'sujets' => $sujets,
        'error_login'=> $authenticationUtils->getLastAuthenticationError()
      )
    );
	}

  public function viewAction(Request $request, $id) {
    $em = $this->getDoctrine()->getManager();
    $forum = $em->getRepository('PLUCoreBundle:Forum')->findOneById($id);
    $messages = $em
      ->getRepository('PLUCoreBundle:Message')
      ->getMessagesByForum($id)
    ;

    if (null == $forum) {
      $request->getSession()->getFlashBag()->add('danger', "Ce forum n'existe pas.");
      return $this->redirectToRoute('plu_core_forum_homepage');
    }

    // affichage du formulaire de création de message
    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
    ){
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_forum_homepage');
    }
    $em = $this->getDoctrine()->getManager();
    $forum = $em->getRepository('PLUCoreBundle:Forum')->findOneById($id);

    $message  = new Message();
    $message->setUtilisateur($this->getUser());
    $message->setForum($forum);

    $form   = $this->get('form.factory')->create(MessageType::class, $message);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($message);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Message bien enregistré.');

      return $this->redirectToRoute('plu_core_forum_view', array('id' => $message->getForum()->getId()));
    }
    return $this->render('PLUCoreBundle:Forum:view.html.twig', array(
      'forum' => $forum,
      'messages' => $messages,
      'form' => $form->createView()
    ));
  }

  public function addAction(Request $request){
    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ANCIEN')
    ){
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_forum_homepage');
    }

    $forum  = new Forum();
    $forum->setCreateur($this->getUser());
    $form   = $this->get('form.factory')->create(ForumType::class, $forum);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($forum);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Forum bien enregistré.');

      return $this->redirectToRoute('plu_core_sujet_view', array('id' => $forum->getSujet()->getId()));
    }
    return $this->render(
      'PLUCoreBundle:Forum:add.html.twig',
      array(
        'form' => $form->createView()
      )
    );
	}

  public function editAction(Request $request, $id){

    $forum = $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('PLUCoreBundle:Forum')
      ->find($id)
    ;

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ANCIEN')
    ) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_forum_view', array('id' => $forum->getId()));
    }

    if (null === $forum) {
      $request->getSession()->getFlashBag()->add('danger', 'Ce forum n\'existe pas.');

      return $this->redirectToRoute('plu_core_forum_homepage');
    }

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && $message->getUtilisateur()->getId() !== $this->getUser()->getId()
    ) { 
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_forum_view', array('id' => $forum->getId()));
    }

    $form = $this->get('form.factory')->create(ForumType::class, $forum);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($forum);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Forum bien enregistré.');

      return $this->redirectToRoute('plu_core_forum_view', array('id' => $forum->getId()));
    }
    return $this->render(
      'PLUCoreBundle:Forum:edit.html.twig',
      array(
        'forum' => $forum,
        'form' => $form->createView()
      )
    );
	}

  public function deleteAction(Request $request, $id){

    $em = $this->getDoctrine()->getManager();
    $forum = $em->getRepository('PLUCoreBundle:Forum')->find($id);

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_forum_view', array('id' => $forum->getId()));
    }

    $idSujet = $forum->getSujet()->getId();

    if (null === $forum) {
      $request->getSession()->getFlashBag()->add('danger', 'Ce forum n\'existe pas');

      return $this->redirectToRoute('plu_core_forum_homepage');
    }

    $form = $this->get('form.factory')->create();

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em->remove($forum);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', "Le forum a bien été supprimé.");

      return $this->redirectToRoute('plu_core_sujet_view', array('id' => $idSujet));
    }

    return $this->render('PLUCoreBundle:Forum:delete.html.twig', array(
      'forum' => $forum,
      'form'   => $form->createView(),
    ));
	}

  public function resoudreAction($id, Request $request){

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ANCIEN')
    ){
      $response = new JsonResponse();
      return $response->setData(
        array('error' => 'Vous n\'avez pas accès à cette fonction')
      );
    }

    $em = $this->getDoctrine()->getManager();
    $forum = $em->getRepository('PLUCoreBundle:Forum')->find($id);

    $forum->setResolu(! $forum->getResolu());
    $em->flush();

    $response = new JsonResponse();
    return $response->setData(
      array('resolu' => $forum->getResolu())
    );

  }

}

?>
