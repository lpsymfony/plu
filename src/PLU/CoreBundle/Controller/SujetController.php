<?php

namespace PLU\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use PLU\CoreBundle\Entity\Sujet;
use PLU\CoreBundle\Entity\Forum;
use PLU\CoreBundle\Form\Sujet\SujetType;

class SujetController extends Controller {

  public function viewAction(Request $request, $id) {
    $em = $this->getDoctrine()->getManager();
    $sujet = $em->getRepository('PLUCoreBundle:Sujet')->findOneById($id);
    $forums = $em
      ->getRepository('PLUCoreBundle:Forum')
      ->getForumBySujet($id)
    ;

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
    ){
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_forum_homepage');
    }

    if (null == $sujet) {
      $request->getSession()->getFlashBag()->add('danger', "Ce sujet n'existe pas.");
      return $this->redirectToRoute('plu_core_forum_homepage');
    }

    return $this->render('PLUCoreBundle:Sujet:view.html.twig', array(
      'sujet' => $sujet,
      'forums' => $forums
    ));
  }

  public function addAction(Request $request){
    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
    ){
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_forum_homepage');
    }

    $sujet  = new Sujet();
    $sujet->setCreateur($this->getUser());
    $form   = $this->get('form.factory')->create(SujetType::class, $sujet);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($sujet);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Sujet bien enregistré.');

      return $this->redirectToRoute('plu_core_forum_homepage');
    }
    return $this->render(
      'PLUCoreBundle:Sujet:add.html.twig',
      array(
        'form' => $form->createView()
      )
    );
	}

  public function editAction(Request $request, $id){

    $sujet = $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('PLUCoreBundle:Sujet')
      ->find($id)
    ;

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_sujet_view', array('id' => $sujet->getId()));
    }

    if (null === $sujet) {
      $request->getSession()->getFlashBag()->add('danger', 'Ce sujet n\'existe pas.');

      return $this->redirectToRoute('plu_core_forum_homepage');
    }

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && $message->getUtilisateur()->getId() !== $this->getUser()->getId()
    ) { 
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_sujet_view', array('id' => $sujet->getId()));
    }

    $form = $this->get('form.factory')->create(SujetType::class, $sujet);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($sujet);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Sujet bien enregistré.');

      return $this->redirectToRoute('plu_core_sujet_view', array('id' => $sujet->getId()));
    }
    return $this->render(
      'PLUCoreBundle:Sujet:edit.html.twig',
      array(
        'sujet' => $sujet,
        'form' => $form->createView()
      )
    );
	}

  public function deleteAction(Request $request, $id){

    $em = $this->getDoctrine()->getManager();
    $sujet = $em->getRepository('PLUCoreBundle:Sujet')->find($id);

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_sujet_view', array('id' => $sujet->getId()));
    }

    if (null === $sujet) {
      $request->getSession()->getFlashBag()->add('danger', 'Ce sujet n\'existe pas');

      return $this->redirectToRoute('plu_core_forum_homepage');
    }

    $form = $this->get('form.factory')->create();

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em->remove($sujet);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', "Le sujet a bien été supprimé.");

      return $this->redirectToRoute('plu_core_forum_homepage');
    }

    return $this->render('PLUCoreBundle:Sujet:delete.html.twig', array(
      'sujet' => $sujet,
      'form'   => $form->createView(),
    ));
	}

}

?>
