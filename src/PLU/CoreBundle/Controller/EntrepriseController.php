<?php

namespace PLU\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use PLU\CoreBundle\Entity\Entreprise;
use PLU\CoreBundle\Form\Entreprise\EntrepriseType;
use PLU\CoreBundle\Form\Entreprise\EntrepriseAddType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Finder\Exception\AccessDeniedException;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EntrepriseController extends Controller {

	public function indexAction(Request $request){
		if (
			false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
			&& false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
			&& false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
		) {
			$request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}
		$listEntreprise = $this->getDoctrine()->getManager()->getRepository('PLUCoreBundle:Entreprise')->findAll();
		return $this->render('PLUCoreBundle:Entreprise:index.html.twig', array('listEntreprise' => $listEntreprise));
	}

	public function addAction(Request $request){
		//Vérifie que l'utilisateur ne soit pas déjà connecté et qu'il ne soit pas un administrateur
		if (
			false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
			&& false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
		) {
			$request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		$entreprise = new Entreprise();
		$form   = $this->get('form.factory')->create(EntrepriseAddType::class, $entreprise);

		if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
			$em = $this->getDoctrine()->getManager();

			$salt = md5(uniqid());
			$encoded = $this->get('security.encoder_factory')->getEncoder($entreprise)->encodePassword($entreprise->getPassword(), $salt);

			$entreprise->setPassword($encoded);
			$entreprise->setSalt($salt);

			$em->persist($entreprise);
			$em->flush();

			$request->getSession()->getFlashBag()->add('notice', 'Entreprise bien enregistrée.');

			//Redirige l'utilisateur sur la liste des entreprises si celui-ci est administrateur, sinon sur la liste des licences
			if (
				$this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
				|| $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
			) {
				return $this->redirectToRoute('plu_core_entreprise_homepage');
			} else {
				return $this->redirectToRoute('plu_core_licence_homepage');
			}

		}
		return $this->render('PLUCoreBundle:Entreprise:add.html.twig',array('form' => $form->createView()));
	}

	public function editAction(Request $request, $id){
		//Vérifie que l'utilisatateur soit connecté
		if (false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')) {
			$request->getSession()->getFlashBag()->add('danger', "Veuillez vous connecter pour accéder à cette page.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}
		$em = $this->getDoctrine()->getManager();
		$entreprise  = $em->getRepository('PLUCoreBundle:Entreprise')->findOneByUsername($id);

		if (null === $entreprise) {
			$request->getSession()->getFlashBag()->add('danger', "Il n'existe pas d'entreprise avec l'e-mail \"$id\".");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		//Vérifie que l'utilisateur essaie de modifier son profil ou bien que l'utilisateur soit un administrateur
		if (
			$this->getUser()->getUsername()!==$entreprise->getUsername()
			&& false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
			&& false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
		) {
			$request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas le droit d'accéder à ce profil.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		$form   = $this->get('form.factory')->create(EntrepriseType::class, $entreprise);

		if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
			$em->persist($entreprise);
			$em->flush();

			$request->getSession()->getFlashBag()->add('notice', 'Profil bien modifié.');

			//Redirige l'utilisateur sur la liste des entreprises si celui-ci est administrateur, sinon sur la liste des licences
			if (
				$this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
				|| $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
			) {
				return $this->redirectToRoute('plu_core_entreprise_homepage');
			} else {
				return $this->redirectToRoute('plu_core_licence_homepage');
			}

		}
		return $this->render('PLUCoreBundle:Entreprise:edit.html.twig', array('form' => $form->createView()));
	}

	public function deleteAction(Request $request, $id){

		//Vérifie que l'utilisateur soit un administrateur
		if (
			false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
			&& false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
		) {
			$request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		$em = $this->getDoctrine()->getManager();
		$entreprise = $em->getRepository('PLUCoreBundle:Utilisateur')->findOneByUsername($id);

		if (null === $entreprise) {
			$request->getSession()->getFlashBag()->add('danger', "Il n'existe pas d'entreprise avec l'e-mail \"$id\".");
			return $this->redirectToRoute('plu_core_entreprise_homepage');
		}

		$form = $this->get('form.factory')->create();

		if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
			$em->remove($entreprise);
			$em->flush();

			$request->getSession()->getFlashBag()->add('notice', "L'entreprise a bien été supprimée.");

			return $this->redirectToRoute('plu_core_entreprise_homepage');
		}

		return $this->render('PLUCoreBundle:Entreprise:delete.html.twig',
		array('entreprise' => $entreprise, 'form' => $form->createView(),));
	}
}
