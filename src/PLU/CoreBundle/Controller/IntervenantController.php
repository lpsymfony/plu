<?php

namespace PLU\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use PLU\CoreBundle\Entity\Intervenant;
use PLU\CoreBundle\Form\Intervenant\IntervenantType;
use PLU\CoreBundle\Form\Intervenant\IntervenantAddType;
use PLU\CoreBundle\Form\Intervenant\AdministrateurAddIntervenantType;

class IntervenantController extends Controller{

  public function indexAction(Request $request){
    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ENTREPRISE')
    ) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    if (
      $this->get('security.authorization_checker')->isGranted('ROLE_ENTREPRISE')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Entreprise"
    ) {
      $listIntervenant = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('PLUCoreBundle:Intervenant')
        ->getIntervenantByEntreprise($this->getUser()->getId())
      ;
    }else {
      $listIntervenant = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('PLUCoreBundle:Intervenant')
        ->getIntervenantAll()
      ;
    }

    return $this->render(
      'PLUCoreBundle:Intervenant:index.html.twig',
      array(
        'listIntervenant' => $listIntervenant
      )
    );
  }

  public function addAction(Request $request){

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ENTREPRISE')
    ) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $em = $this->getDoctrine()->getManager();

    $intervenant = new Intervenant();
    if (
      $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Administrateur"
    ){
      $form = $this->get('form.factory')->create(AdministrateurAddIntervenantType::class, $intervenant);
    }elseif(
      $this->get('security.authorization_checker')->isGranted('ROLE_ENTREPRISE')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Entreprise"
    ){
      $entreprise = $em->getRepository('PLUCoreBundle:Entreprise')->find($this->getUser()->getId());
      $intervenant->setEntreprise($entreprise);
      $form = $this->get('form.factory')->create(IntervenantAddType::class, $intervenant);
    }

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {

      $salt = md5(uniqid());
      $encoded = $this->get('security.encoder_factory')->getEncoder($intervenant)->encodePassword($intervenant->getPassword(), $salt);

      //Ajout de l'enseignant dans la table générique enseignant
      $intervenant->setPassword($encoded);
      $intervenant->setSalt($salt);

      $em->persist($intervenant);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Inscription bien enregistrée.');

      return $this->redirectToRoute('plu_core_intervenant_homepage');
    }
    return $this->render(
      'PLUCoreBundle:Intervenant:add.html.twig',
      array(
        'form' => $form->createView(),
      )
    );
  }

  public function editAction(Request $request, $id){
		//Vérifie que l'utilisatateur soit connecté
		if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ENTREPRISE')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT')
    ) {
			$request->getSession()->getFlashBag()->add('danger', "Veuillez vous connecter pour accéder à cette page.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}
		$em = $this->getDoctrine()->getManager();
		$user  = $em->getRepository('PLUCoreBundle:Utilisateur')->findOneByUsername($id);
		$entreprise  = $em->getRepository('PLUCoreBundle:Intervenant')->findOneByUsername($id);

		if (null === $entreprise || null === $user) {
			$request->getSession()->getFlashBag()->add('danger', "Il n'existe pas d'intervenant avec l'e-mail \"$id\".");
      if (
        $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT')
        && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Intervenant"
      ) {
        return $this->redirectToRoute('plu_core_licence_homepage');
      }
			return $this->redirectToRoute('plu_core_intervenant_homepage');
		}

    //------------------------------------------------------------------------------------------------------
    $idEntrepriseIntervanant = -1;
    $identreprise = -1;

    if (
      $this->get('security.authorization_checker')->isGranted('ROLE_ENTREPRISE')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Entreprise"
    ) {
      $idEntrepriseIntervanant = $this->getUser()->getId();
      $identreprise = $user->getEntreprise()->getId();
    }

		if (
      $this->getUser()->getUsername()!==$user->getUsername()
		  && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && $idEntrepriseIntervanant !== $identreprise
    ) {
			$request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas le droit d'accéder à ce profil.");
      if (
        $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT')
        && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Intervenant"
      ) {
        return $this->redirectToRoute('plu_core_licence_homepage');
      }
			return $this->redirectToRoute('plu_core_intervenant_homepage');
		}
    //--------------------------------------------------------------------------------------------------------

		$form   = $this->get('form.factory')->create(IntervenantType::class, $entreprise);

		if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
			$em->persist($entreprise);
			$em->flush();

			$request->getSession()->getFlashBag()->add('notice', 'Profil bien modifié.');

      if (
        $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT')
        && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Intervenant"
      ) {
        return $this->redirectToRoute('plu_core_licence_homepage');
      }
			return $this->redirectToRoute('plu_core_intervenant_homepage');
		}

		return $this->render('PLUCoreBundle:Intervenant:edit.html.twig', array('form' => $form->createView()));
	}

  public function deleteAction(Request $request, $id){

		//Vérifie que l'utilisateur soit un administrateur
		if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ENTREPRISE')
    ) {
			$request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
			return $this->redirectToRoute('plu_core_licence_homepage');
		}

		$em = $this->getDoctrine()->getManager();
		$intervenant = $em->getRepository('PLUCoreBundle:Intervenant')->findOneByUsername($id);

		if (null === $intervenant) {
			$request->getSession()->getFlashBag()->add('danger', "Il n'existe pas d'intervenant avec l'e-mail \"$id\".");
			return $this->redirectToRoute('plu_core_intervenant_homepage');
		}

    //-------------------------------------------------------------------------------------------------
    if (
      $this->get('security.authorization_checker')->isGranted('ROLE_ENTREPRISE')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Entreprise"
    ) {
      $idEntrepriseIntervanant = $this->getUser()->getId();
      $identreprise = $intervenant->getEntreprise()->getId();
      if (
        $idEntrepriseIntervanant !== $identreprise
      ) {
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas le droit d'accéder à ce profil.");
        return $this->redirectToRoute('plu_core_intervenant_homepage');
      }
    }
    //--------------------------------------------------------------------------------------------

		$form = $this->get('form.factory')->create();

		if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
			$em->remove($intervenant);
			$em->flush();
			$request->getSession()->getFlashBag()->add('notice', "Le profil a bien été supprimé.");
			return $this->redirectToRoute('plu_core_intervenant_homepage');
		}

		return $this->render(
      'PLUCoreBundle:Intervenant:delete.html.twig',
  		array(
        'intervenant' => $intervenant,
        'form' => $form->createView()
      )
    );
	}

}
