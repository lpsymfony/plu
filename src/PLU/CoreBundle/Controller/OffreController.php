<?php

namespace PLU\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Finder\Exception\AccessDeniedException;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use PLU\CoreBundle\Entity\Offre;
use PLU\CoreBundle\Form\Offre\OffreType;
use PLU\CoreBundle\Form\Offre\AdministrateurOffreType;
use PLU\CoreBundle\Form\Offre\AdministrateurAddOffreType;
use Symfony\Component\Validator\Constraints\File;

class OffreController extends Controller{

  public function modifAcceptAction($id, Request $request){

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
    ){
      $response = new JsonResponse();
      return $response->setData(
        array('error' => 'Vous n\'avez pas accès à cette fonction')
      );
    }

    $em = $this->getDoctrine()->getManager();
    $offre = $em->getRepository('PLUCoreBundle:Offre')->getOffre($id);

    $offre->setValide(! $offre->getValide());
    $em->flush();

    $response = new JsonResponse();
    return $response->setData(
      array('valide' => $offre->getValide())
    );

  }

  public function modifVisibleAction($id, Request $request){

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT')
    ){
      $response = new JsonResponse();
      return $response->setData(
        array('error' => 'Vous n\'avez pas accès à cette fonction')
      );
    }

    $em = $this->getDoctrine()->getManager();
    $offre = $em->getRepository('PLUCoreBundle:Offre')->getOffre($id);

    $offre->setPublie(! $offre->getPublie());
    $em->flush();

    $response = new JsonResponse();
    return $response->setData(
      array('publie' => $offre->getPublie())
    );

  }

  public function indexAction($page, Request $request){

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ANCIEN')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT')
    ){
      //throw new AccessDeniedException("Vous n'avez pas l'accès à cette page");
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }


    if(
      $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      || $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT')
      || $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
    ){
      $limit = 100;
    }else {
      $limit = 3;
    }
    $nbAdv = null;
    $nbPage = null;

    $em = $this->getDoctrine()->getManager();

    if (
      $this->get('security.authorization_checker')->isGranted('ROLE_ENTREPRISE')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Entreprise"
    ) {
      $nbOffres = $em
        ->getRepository('PLUCoreBundle:Offre')
        ->countOffresByEntreprise($this->getUser()->getId())
      ;
    }elseif(
      $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Intervenant"
    ) {
      $nbOffres = $em
        ->getRepository('PLUCoreBundle:Offre')
        ->countOffresByEntreprise($this->getUser()->getEntreprise()->getId())
      ;
    }elseif (
      $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
    ) {
      $nbOffres = $em
        ->getRepository('PLUCoreBundle:Offre')
        ->countOffresForFuturEtudiant()
      ;
    }elseif (
      $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
    ) {
      $nbOffres = $em
        ->getRepository('PLUCoreBundle:Offre')
        ->countOffresForActuelEtudiant()
      ;
    }elseif (
      $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ANCIEN')
    ) {
      $nbOffres = $em
        ->getRepository('PLUCoreBundle:Offre')
        ->countOffresForAncienEtudiant()
      ;
    }else{
      $nbOffres = $em
        ->getRepository('PLUCoreBundle:Offre')
        ->countOffres()
      ;
    }

    $nbPage = ceil($nbOffres/$limit);

    if ($page < 1 || $page > $nbPage) {
      // throw new NotFoundHttpException('Page "'.$page.'" inexistante.');
      $page=1;
    }

    if (
      $this->get('security.authorization_checker')->isGranted('ROLE_ENTREPRISE')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Entreprise"
    ) {
      $offres = $em
      ->getRepository('PLUCoreBundle:Offre')
      ->getOffresByEntreprise($page,$limit,$this->getUser()->getId())
      ;
    } elseif (
      $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Intervenant"
    ) {
      $offres = $em
      ->getRepository('PLUCoreBundle:Offre')
      ->getOffresByEntreprise($page,$limit,$this->getUser()->getEntreprise()->getId())
      ;
    }elseif (
      $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
    ) {
      $offres = $em
      ->getRepository('PLUCoreBundle:Offre')
      ->getOffresForFuturEtudiant($page,$limit)
      ;
    }elseif (
      $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
    ) {
      $offres = $em
      ->getRepository('PLUCoreBundle:Offre')
      ->getOffresForActuelEtudiant($page,$limit)
      ;
    }elseif (
      $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ANCIEN')
    ) {
      $offres = $em
      ->getRepository('PLUCoreBundle:Offre')
      ->getOffresForAncienEtudiant($page,$limit)
      ;
    }else{
      $offres = $em
      ->getRepository('PLUCoreBundle:Offre')
      ->getOffres($page,$limit)
      ;
    }

    // // TODO: resoudre le probleme avec pagination
    // $offres = $em
    //   ->getRepository('PLUCoreBundle:Offre')
    //   ->getOffreAll()
    // ;

    return $this->render('PLUCoreBundle:Offre:index.html.twig', array(
      'offres' => $offres,
      'nbPages' => $nbPage,
      'page' => $page
    ));
  }

  public function viewAction($id, Request $request){

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ANCIEN')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT')
    ) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $em = $this->getDoctrine()->getManager();
    $offre = $em->getRepository('PLUCoreBundle:Offre')->getOffre($id);

    if (null === $offre) {
      $request->getSession()->getFlashBag()->add('danger', "L'offre n'existe pas.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    if ( $offre->getCategorie()->getNom() == "Stage" ) {
      if (
        $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ANCIEN')
        ||$this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
      ){
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
        return $this->redirectToRoute('plu_core_offre_homepage');
      }
    }elseif ( $offre->getCategorie()->getNom() == "CDD" ) {
      if (
        $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
      ){
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
        return $this->redirectToRoute('plu_core_offre_homepage');
      }
    }elseif ( $offre->getCategorie()->getNom() == "CDI" ) {
      if (
        $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
      ){
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
        return $this->redirectToRoute('plu_core_offre_homepage');
      }
    }elseif ( $offre->getCategorie()->getNom() == "Alternance contrat d'apprentissage" ) {
      if (
        $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
        ||$this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ANCIEN')
      ){
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
        return $this->redirectToRoute('plu_core_offre_homepage');
      }
    }elseif ( $offre->getCategorie()->getNom() == "Alternance contrat de professionnalisation" ) {
      if (
        $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
        ||$this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ANCIEN')
      ){
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
        return $this->redirectToRoute('plu_core_offre_homepage');
      }
    }

    if (
      $this->get('security.authorization_checker')->isGranted('ROLE_ENTREPRISE')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Entreprise"
    ) {
      if ($offre->getEntreprise()->getId() != $this->getUser()->getId()) {
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
        return $this->redirectToRoute('plu_core_offre_homepage');
      }
    } elseif (
      $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Intervenant"
    ) {
      if ($offre->getEntreprise()->getId() != $this->getUser()->getEntreprise()->getId()) {
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
        return $this->redirectToRoute('plu_core_offre_homepage');
      }
    }

    return $this->render('PLUCoreBundle:Offre:view.html.twig', array(
      'offre' => $offre
    ));
  }

  public function addAction(Request $request){

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT')
    ){
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $em = $this->getDoctrine()->getManager();


    $offre  = new Offre();
    if ( $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT') ){
      $form   = $this->get('form.factory')->create(AdministrateurAddOffreType::class, $offre);
    }
    elseif( $this->get('security.authorization_checker')->isGranted('ROLE_ENTREPRISE')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Entreprise" ) {
      $entreprise = $em->getRepository('PLUCoreBundle:Entreprise')->find($this->getUser()->getId());
      $offre->setEntreprise($entreprise);
      $offre->setValide(false);
      $form   = $this->get('form.factory')->create(OffreType::class, $offre);
    } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Intervenant" ) {
        $offre->setEntreprise($this->getUser()->getEntreprise());
        $offre->setValide(false);
        $form = $this->get('form.factory')->create(OffreType::class, $offre);
    }


    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($offre);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Offre bien enregistrée.');

      return $this->redirectToRoute('plu_core_offre_homepage');
    }
    return $this->render(
      'PLUCoreBundle:Offre:add.html.twig',
      array(
        'form' => $form->createView()
      )
    );
  }

  public function editAction($id, Request $request){

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT'))
    {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_offre_homepage');
    }

    $em = $this->getDoctrine()->getManager();
    $offre = $em->getRepository('PLUCoreBundle:Offre')->getOffre($id);

    if (null === $offre) {
      $request->getSession()->getFlashBag()->add('danger', "L'offre que vous tentez de modifier n'existe plus.");
      return $this->redirectToRoute('plu_core_offre_homepage');
    }

    if (
      $this->get('security.authorization_checker')->isGranted('ROLE_ENTREPRISE')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Entreprise"
      && $offre->getEntreprise()->getId() == $this->getUser()->getId()
    ) {
      $offre->setValide(false);
      $form = $this->get('form.factory')->create(OffreType::class, $offre);
    } elseif (
      $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Intervenant"
      && $offre->getEntreprise()->getId() == $this->getUser()->getEntreprise()->getId()
    ) {
      $offre->setValide(false);
      $form = $this->get('form.factory')->create(OffreType::class, $offre);
    } elseif (
      $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
    ) {
      $form = $this->get('form.factory')->create(AdministrateurOffreType::class, $offre);
    }else {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_offre_homepage');
    }

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $offre->preUpload();
      $em->persist($form->getData());
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', "L'offre a bien été modifiée.");

      return $this->redirectToRoute('plu_core_offre_homepage');
    }

    return $this->render(
      'PLUCoreBundle:Offre:add.html.twig',
      array(
        'offre' => $offre,
        'form' => $form->createView()
      )
    );
  }

  public function deleteAction($id, Request $request){
    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT'))
    {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $em = $this->getDoctrine()->getManager();
    $offre = $em->getRepository('PLUCoreBundle:Offre')->getOffre($id);

    if (null === $offre) {
      $request->getSession()->getFlashBag()->add('danger', "L'offre que vous tentez de supprimer n'existe pas.");
      return $this->redirectToRoute('plu_core_offre_homepage');
    }

    if (
      $this->get('security.authorization_checker')->isGranted('ROLE_ENTREPRISE')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Entreprise"
      && $offre->getEntreprise()->getId() != $this->getUser()->getId()
      || $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Intervenant"
      && $offre->getEntreprise()->getId() != $this->getUser()->getEntreprise()->getId()
    ) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_offre_homepage');
    }

    $form = $this->get('form.factory')->create();

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em->remove($offre);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', "L'offre a bien été supprimée.");

      return $this->redirectToRoute('plu_core_offre_homepage');
    }

    return $this->render(
      'PLUCoreBundle:Offre:delete.html.twig',
      array(
        'offre' => $offre,
        'form' => $form->createView()
      )
    );

  }

  public function downloadAction($id, Request $request){

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ANCIEN')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT'))
    {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $em = $this->getDoctrine()->getManager();
    $offre = $em->getRepository('PLUCoreBundle:Offre')->find($id);

    if (null === $offre) {
      throw new NotFoundHttpException("L'offre d'id ".$id." n'existe pas.");
    }

    if (
      $this->get('security.authorization_checker')->isGranted('ROLE_ENTREPRISE')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Entreprise"
    ) {
      if ($offre->getEntreprise()->getId() != $this->getUser()->getId()) {
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
        return $this->redirectToRoute('plu_core_offre_homepage');
      }
    } elseif (
      $this->get('security.authorization_checker')->isGranted('ROLE_INTERVENANT')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Intervenant"
    ) {
      if ($offre->getEntreprise()->getId() != $this->getUser()->getEntreprise()->getId()) {
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
        return $this->redirectToRoute('plu_core_offre_homepage');
      }
    }
    // TODO: etudiant download offre

    if ( $offre->getCategorie()->getNom() == "Stage" ) {
      if (
        $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ANCIEN')
        ||$this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
      ){
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
        return $this->redirectToRoute('plu_core_offre_homepage');
      }
    }elseif ( $offre->getCategorie()->getNom() == "CDD" ) {
      if (
        $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ANCIEN')
        || $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
      ){
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
        return $this->redirectToRoute('plu_core_offre_homepage');
      }
    }elseif ( $offre->getCategorie()->getNom() == "CDI" ) {
      if (
        $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ANCIEN')
        || $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
      ){
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
        return $this->redirectToRoute('plu_core_offre_homepage');
      }
    }elseif ( $offre->getCategorie()->getNom() == "Alternance contrat d'apprentissage" ) {
      if (
        $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
      ){
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
        return $this->redirectToRoute('plu_core_offre_homepage');
      }
    }elseif ( $offre->getCategorie()->getNom() == "Alternance contrat de professionnalisation" ) {
      if (
        $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_FUTUR')
      ){
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
        return $this->redirectToRoute('plu_core_offre_homepage');
      }
    }

    $fichiers = $offre->getFichiers();

    if (null === $fichiers) {
      throw new NotFoundHttpException("Cette offre ne possède pas de pièce jointe.");
    }

    $path = $offre->getWebPath();

    if (!file_exists($path)){
      throw new NotFoundHttpException("Le fichier est introuvable.");
    }

    $filename = dirname($path);
    $size = filesize($path);

    $response = new Response();
    $response->setStatusCode(200);
    $response->headers->set('Content-Type', "application/force_download");
    $response->headers->set('Content-Disposition', 'attachment; filename="'.$fichiers.'"');
    $response->setContent(file_get_contents($path));
    $response->setCharset('UTF-8');

    $response->send();
    return $response;

  }
}
