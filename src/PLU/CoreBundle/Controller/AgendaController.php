<?php

namespace PLU\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use PLU\CoreBundle\Form\Agenda\AgendaType;
use PLU\CoreBundle\Form\Agenda\AgendaEtudiantType;
use PLU\CoreBundle\Form\Agenda\AgendaAffichageType;
use PLU\CoreBundle\Entity\Agenda;

class AgendaController extends Controller{

  public function indexAction($dateParam = null, $licenceId = null, Request $request){

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $em=$this->getDoctrine()->getManager();
    $licences = $em->getRepository('PLUCoreBundle:Licence')->findAll();

    if (!$licences) {
      $request->getSession()->getFlashBag()->add('danger', "Impossible d'afficher l'agenda car il n'existe pas encore de licence.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $formAgenda = $this->get('form.factory')->create(AgendaAffichageType::class);
    if ($licenceId === null) {
      $licenceId = $licences[0]->getId();
    }

    $discr = $em->getMetadataFactory()->getMetadataFor(get_class($this->getUser()))->discriminatorValue;
    if ($discr === "etudiant") {
      $licenceId = $this->getUser()->getLicence()->getId();
    } 
    $licence = $em->getRepository('PLUCoreBundle:Licence')->find($licenceId);
    $formAgenda->get('licence')->setData($licence);
    
    if ($dateParam === null) {
      $dateParam = date('Y-m');
    }
    $formAgenda->get('date')->setData($dateParam);

    $agendas = $em->getRepository('PLUCoreBundle:Agenda')->getAgendaByLicenceAndDate($licenceId, $dateParam);

    return $this->render(
      'PLUCoreBundle:Agenda:index.html.twig',
      array(
        'formAgenda' => $formAgenda->createView(),
        'agendas' => $agendas,
        'discr' => $discr,
        'licence' => $licence,
      )
    );
  }

  public function addAction(Request $request){

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $agenda = new Agenda();

    $em = $this->getDoctrine()->getManager();
    $discr = $em->getMetadataFactory()->getMetadataFor(get_class($this->getUser()))->discriminatorValue;
    
    if ($discr === "etudiant") {
      $form = $this->get('form.factory')->create(AgendaEtudiantType::class, $agenda);
    } else {
      $form = $this->get('form.factory')->create(AgendaType::class, $agenda);
    }

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) { 
      if ($discr === "etudiant") {
        $agenda->addLicence($this->getUser()->getLicence());
      } 
      $agenda->setUtilisateur($this->getUser());
      $em->persist($agenda);
      $em->flush();
      $request->getSession()->getFlashBag()->add('notice', 'Agenda bien enregistrée.');
      return $this->redirectToRoute('plu_core_agenda_homepage');
    }
    return $this->render(
      'PLUCoreBundle:Agenda:add.html.twig',
      array(
        'discr' => $discr,
        'form' => $form->createView(),
      )
    );
  }

  public function editAction(Request $request, $id){

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $em = $this->getDoctrine()->getManager();
    $agenda  = $em->getRepository('PLUCoreBundle:Agenda')->find($id);

    if (null === $agenda) {
      $request->getSession()->getFlashBag()->add('danger', 'Cette agenda n\'existe pas.');
      return $this->redirectToRoute('plu_core_agenda_homepage');
    }

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
      if (null === $agenda->getUtilisateur()->getId()) {
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
        return $this->redirectToRoute('plu_core_licence_homepage');
      }
      else {
        if (($this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
          && (0 === count(array_intersect($agenda->getLicencesArray(), $this->getUser()->getLicencesArray()))
          && $agenda->getUtilisateur()->getId() !== $this->getUser()->getId()))
        || (($this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
          || $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL'))
          && false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
          && $agenda->getUtilisateur()->getId() !== $this->getUser()->getId())) {
            $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
            return $this->redirectToRoute('plu_core_licence_homepage');
        }
      }
    }

    $form = $this->get('form.factory')->create(AgendaType::class, $agenda);
    $discr = $em->getMetadataFactory()->getMetadataFor(get_class($this->getUser()))->discriminatorValue;

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em->persist($agenda);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Agenda bien enregistrée.');

      return $this->redirectToRoute('plu_core_agenda_homepage');
    }
    return $this->render(
      'PLUCoreBundle:Agenda:add.html.twig',
      array(
        'discr' => $discr,
        'form' => $form->createView(),
      )
    );
  }

  public function deleteAction(Request $request, $id) {

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $em = $this->getDoctrine()->getManager();
    $agenda = $em->getRepository('PLUCoreBundle:Agenda')->find($id);

    if (null === $agenda) {
      $request->getSession()->getFlashBag()->add('danger', 'Cette agenda n\'existe pas');
      return $this->redirectToRoute('plu_core_agenda_homepage');
    }

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
      if (null === $agenda->getUtilisateur()->getId()) {
        $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
        return $this->redirectToRoute('plu_core_licence_homepage');
      }
      else {
        if (($this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
          && (0 === count(array_intersect($agenda->getLicencesArray(), $this->getUser()->getLicencesArray()))
          && $agenda->getUtilisateur()->getId() !== $this->getUser()->getId()))
        || (($this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
          || $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL'))
          && false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
          && $agenda->getUtilisateur()->getId() !== $this->getUser()->getId())) {
            $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
            return $this->redirectToRoute('plu_core_licence_homepage');
        }
      }
    }

    $form = $this->get('form.factory')->create();

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em->remove($agenda);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', "L'agenda a bien été supprimée.");
      return $this->redirectToRoute('plu_core_agenda_homepage');
    }

    return $this->render('PLUCoreBundle:Agenda:delete.html.twig', array(
      'agenda' => $agenda,
      'form'   => $form->createView(),
    ));
  }
}
