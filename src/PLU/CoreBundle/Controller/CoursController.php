<?php

namespace PLU\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use PLU\CoreBundle\Entity\Cours;
use Symfony\Component\HttpFoundation\JsonResponse; 
use PLU\CoreBundle\Form\Cours\CoursAddType;
use PLU\CoreBundle\Form\Cours\CoursEditType;
use PLU\CoreBundle\Form\Cours\CoursCreateFolderType;
use PLU\CoreBundle\Form\Cours\CoursEditFolderType;

class CoursController extends Controller{

  public function indexAction(Request $request){

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $em = $this->getDoctrine()->getManager();
    $cours  = new Cours();
    $cours->createRootDirIfDoesntExist();

    $listCours = $em->getRepository('PLUCoreBundle:Cours')->findAll();
    $listCours = $cours->coursArrayToAssociativeArray($listCours);
    $chemins = $cours->dirToArray("cours", $listCours);

    $allPaths = [];


    if ($this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')) {
      $allPathsResponse = $em->getRepository('PLUCoreBundle:Cours')->getAllPathNameByLicence($this->getUser()->getLicence()->getId());
      foreach ($allPathsResponse as $key => $value) {
        $allPaths[] = $value["chemin"];
      }
    }

    return $this->render('PLUCoreBundle:Cours:index.html.twig', array(
      'chemins' => $chemins,
      'DS' => DIRECTORY_SEPARATOR,
      'allPaths' => $allPaths
      )
    );
  }

  public function viewAction($id, Request $request){
    
    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $em = $this->getDoctrine()->getManager();
    $cours = $em->getRepository('PLUCoreBundle:Cours')->find($id);

    if (null === $cours) {
      $request->getSession()->getFlashBag()->add('danger', "Ce cours n'existe pas.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    if ($this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
    && !in_array($this->getUser()->getLicence()->getId(), $cours->getLicencesArray())) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    return $this->render(
      'PLUCoreBundle:Cours:view.html.twig',
      array('cours' => $cours)
    );
  }

  public function addAction(Request $request){

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    $em = $this->getDoctrine()->getManager();

    $cours  = new Cours();
    $cours->createRootDirIfDoesntExist();
    $form   = $this->get('form.factory')->create(CoursAddType::class, $cours);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $cours->setUtilisateur($this->getUser());
      $em->persist($cours);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Cours bien enregistrée.');

      return $this->redirectToRoute('plu_core_cours_view',
        array('id' => $cours->getId()));
    }
    return $this->render(
      'PLUCoreBundle:Cours:add.html.twig',
      array(
        'form' => $form->createView()
      )
    );
  }

  public function editAction($id, $oldPath, $oldFichiers, Request $request){

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    $oldPath = str_replace(".", "/", $oldPath);
    $em = $this->getDoctrine()->getManager();
    $cours = $em->getRepository('PLUCoreBundle:Cours')->find($id);

    if (null === $cours) {
      $request->getSession()->getFlashBag()->add('danger', "Le cours que vous tentez de modifier n'existe plus.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }
    
    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
        && $this->getUser()->getId() !== $cours->getUtilisateur()->getId())
    {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    $form = $this->get('form.factory')->create(CoursEditType::class, $cours);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $newCours = clone($cours);

      if (null === $newCours->getFile()) {
        $newCours->setOldId($id);
        $newCours->setOldPath($oldPath);
      }
      $em->persist($newCours);
      
      $cours->setOldId($id);
      $cours->setOldPath($oldPath);
      $cours->setOldFichiers($oldFichiers);
      $em->remove($cours);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', "Le cours a bien été modifiée.");

      return $this->redirectToRoute('plu_core_cours_view',
        array('id' => $newCours->getId()));
    }

    return $this->render(
      'PLUCoreBundle:Cours:add.html.twig',
      array(
        'cours' => $cours,
        'form' => $form->createView()
      )
    );
  }

  public function deleteAction($id, Request $request){
    
    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    $em = $this->getDoctrine()->getManager();
    $cours = $em->getRepository('PLUCoreBundle:Cours')->find($id);

    if (null === $cours) {
      $request->getSession()->getFlashBag()->add('danger', "Le cours que vous tentez de supprimer n'existe pas.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
        && $this->getUser()->getId() !== $cours->getUtilisateur()->getId())
    {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    $form = $this->get('form.factory')->create();

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em->remove($cours);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', "Le cours a bien été supprimée.");

      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    return $this->render(
      'PLUCoreBundle:Cours:delete.html.twig',
      array(
        'cours' => $cours,
        'form' => $form->createView()
      )
    );
  }

  public function createFolderAction(Request $request){

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    $em = $this->getDoctrine()->getManager();

    $form   = $this->get('form.factory')->create(CoursCreateFolderType::class);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $cours = new Cours();
      $cours->createRootDirIfDoesntExist();
      $createSucces = $cours->createFolder($form->getData()["chemin"]."/".$form->getData()["folder"]);

      if ($createSucces) {
        $request->getSession()->getFlashBag()->add('notice', "Le dossier a été créé.");
      } else {
        $request->getSession()->getFlashBag()->add('danger', "Le dossier n'a pas été créé car il existe déjà.");
      }

      return $this->redirectToRoute('plu_core_cours_homepage');
    }
    return $this->render(
      'PLUCoreBundle:Cours:createFolder.html.twig',
      array(
        'form' => $form->createView()
      )
    );
  }

  public function deleteFolderAction($path, Request $request){
    
    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    $em = $this->getDoctrine()->getManager();
    $listCoursForPath = $em->getRepository('PLUCoreBundle:Cours')->getAllPathLike($path);

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && count($listCoursForPath) > 0) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    $form = $this->get('form.factory')->create();
    $path = str_replace(".", "/", $path);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $cours = $em->getRepository('PLUCoreBundle:Cours')->getAllPathLike($path);
      foreach ($cours as $key => $value) {
        $em->remove($value);
      }
      $em->flush();
      $coursFolder = new Cours();
      $deleteSucces = $coursFolder->deleteFolder($path);

      if ($deleteSucces) {
        $request->getSession()->getFlashBag()->add('notice', "Le dossier a bien été supprimée.");
      } else {
        $request->getSession()->getFlashBag()->add('danger', "Le dossier n'a pas été supprimée car il n'existe pas.");
      }


      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    return $this->render(
      'PLUCoreBundle:Cours:deleteFolder.html.twig',
      array(
        'path' => $path,
        'form' => $form->createView()
      )
    );
  }

  public function editFolderAction($path, Request $request){

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    $em = $this->getDoctrine()->getManager();
    $listCoursForPath = $em->getRepository('PLUCoreBundle:Cours')->getAllPathLike($path);

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && count($listCoursForPath) > 0) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    $path = str_replace(".", "/", $path);
    $explodeArray = explode("/", $path);
    $folderName = end($explodeArray);
    $folderNameArray["path"] = $folderName;
    $form = $this->get('form.factory')->create(CoursEditFolderType::class, $folderNameArray);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $cours = $em->getRepository('PLUCoreBundle:Cours')->getAllPathLike($path);
      $newFolderName = $form->getData()["path"];
      $newPath = rtrim($path, $folderName).$newFolderName;
      $coursFolder = new Cours();

      $deleteSucces = $coursFolder->editFolder($path, $newPath);

      if ($deleteSucces) {
        $request->getSession()->getFlashBag()->add('notice', "Le dossier a bien été modifié.");
      } else {
        $request->getSession()->getFlashBag()->add('danger', "Le dossier ne peut pas être modifié car il n'existe pas.");
      }

      foreach ($cours as $key => $value) {
        $newCoursPath = $newPath.ltrim($value->getChemin(), $path);
        $value->setChemin($newCoursPath);
      }

      $em->flush();
      
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    return $this->render(
      'PLUCoreBundle:Cours:editFolder.html.twig',
      array(
        'path' => $path,
        'form' => $form->createView()
      )
    );
  }

  public function downloadAction($id, Request $request){
    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $em = $this->getDoctrine()->getManager();
    $cours = $em->getRepository('PLUCoreBundle:Cours')->find($id);

    if (null === $cours) {
      throw new NotFoundHttpException();
      $request->getSession()->getFlashBag()->add('danger', "Le cours n'existe pas.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    if ($this->get('security.authorization_checker')->isGranted('ROLE_ETUDIANT_ACTUEL')
    && !in_array($this->getUser()->getLicence()->getId(), $cours->getLicencesArray())) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    $fichiers = $cours->getFichiers();
    $path = $cours->getWebPath();

    if (!file_exists($path)){
      $request->getSession()->getFlashBag()->add('danger', "Le cours est introuvable.");
      return $this->redirectToRoute('plu_core_cours_homepage');
    }

    $filename = dirname($path);
    $size = filesize($path);

    $response = new Response();
    $response->setStatusCode(200);
    $response->headers->set('Content-Type', "application/force_download");
    $response->headers->set('Content-Disposition', 'attachment; filename="'.$fichiers.'"');
    $response->setContent(file_get_contents($path));
    $response->setCharset('UTF-8');

    $response->send();
    return $response;

  }

  public function modifVisibleAction($id, Request $request){

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ENSEIGNANT')) {
      $response = new JsonResponse();
      return $response->setData(
        array('error' => 'Vous n\'avez pas accès à cette fonction')
      );
    }

    $em = $this->getDoctrine()->getManager();
    $cours = $em->getRepository('PLUCoreBundle:Cours')->find($id);

    if (null === $cours) {
      $response = new JsonResponse();
      return $response->setData(
        array('error' => 'Le cours que vous tentez de modifier n\'existe plus.')
      );
    }
    
    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
        && $this->getUser()->getId() !== $cours->getUtilisateur()->getId())
    {
      $response = new JsonResponse();
      return $response->setData(
        array('error' => 'Vous n\'avez pas accès à cette fonction')
      );
    }

    $cours->setVisible(!$cours->getVisible());
    $em->flush();

    $response = new JsonResponse();
    return $response->setData(
      array('visible' => $cours->getVisible())
    );

  }
}
