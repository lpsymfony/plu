<?php

namespace PLU\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

use PLU\CoreBundle\Entity\Licence;
use PLU\CoreBundle\Form\Licence\LicenceType;
use PLU\CoreBundle\Form\Licence\LicenceEditType;

class LicenceController extends Controller{

  public function menuAction(){
    $em = $this->getDoctrine()->getManager();

    // On récupère les annonces
    $licences = $em->getRepository('PLUCoreBundle:Licence')
      ->findAll()
    ;

    return $this->render('PLUCoreBundle:Licence:menu.html.twig', array(
      'licences' => $licences
    ));
  }

  public function indexAction(){

    $licences = $this
    ->getDoctrine()
    ->getManager()
    ->getRepository('PLUCoreBundle:Licence')
    ->getLicencesEtResponsables()
    ;

    $authenticationUtils = $this->get('security.authentication_utils');
    return $this->render(
      'PLUCoreBundle:Licence:index.html.twig',
      array(
        'licences' => $licences,
        'error_login'=> $authenticationUtils->getLastAuthenticationError()
      )
    );
  }

  public function viewAction($id, Request $request){

    $em = $this->getDoctrine()->getManager();
    $licence = $em->getRepository('PLUCoreBundle:Licence')->findOneById($id);

    if (null == $licence) {
      $request->getSession()->getFlashBag()->add('danger', "Cette licence n'existe pas.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    return $this->render('PLUCoreBundle:Licence:view.html.twig', array(
      'licence' => $licence
    ));
  }

  public function addAction(Request $request){

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $licence  = new Licence();
    $form   = $this->get('form.factory')->create(LicenceType::class, $licence);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($licence);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Licence bien enregistrée.');

      return $this->redirectToRoute('plu_core_licence_homepage');
    }
    return $this->render(
      'PLUCoreBundle:Licence:add.html.twig',
      array(
        'form' => $form->createView()
      )
    );
  }

  public function editAction(Request $request, $id){

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
    && false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $licence  = $this
    ->getDoctrine()
    ->getManager()
    ->getRepository('PLUCoreBundle:Licence')
    ->find($id)
    ;

    if (null === $licence) {
      $request->getSession()->getFlashBag()->add('danger', 'Cette licence n\'existe pas.');

      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    if (
      $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && get_class($this->getUser()) == "PLU\CoreBundle\Entity\Enseignant"
      && $this->getUser()->getLicences()->contains($licence) === false
    ) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
      $form = $this->get('form.factory')->create(LicenceEditType::class, $licence);
    }else {
      $form = $this->get('form.factory')->create(LicenceType::class, $licence);
    }


    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($licence);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Licence bien enregistrée.');

      return $this->redirectToRoute('plu_core_licence_homepage');
    }
    return $this->render(
      'PLUCoreBundle:Licence:add.html.twig',
      array(
        'form' => $form->createView()
      )
    );
  }

  public function deleteAction(Request $request, $id) {

    if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $em = $this->getDoctrine()->getManager();

    $licence = $em->getRepository('PLUCoreBundle:Licence')->find($id);

    if (null === $licence) {
      $request->getSession()->getFlashBag()->add('danger', 'Cette licence n\'existe pas');

      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $form = $this->get('form.factory')->create();

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em->remove($licence);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', "La licence a bien été supprimée.");

      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    return $this->render('PLUCoreBundle:Licence:delete.html.twig', array(
      'licence' => $licence,
      'form'   => $form->createView(),
    ));
  }
}
