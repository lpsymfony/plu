<?php

namespace PLU\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use PLU\CoreBundle\Entity\Page;
use PLU\CoreBundle\Form\Page\PageType;

class PageController extends Controller
{

  public function menuAction(){
    $em = $this->getDoctrine()->getManager();

    // On récupère les annonces
    $pages = $em->getRepository('PLUCoreBundle:Page')->findAll();

    return $this->render('PLUCoreBundle:Page:menu.html.twig', array(
      'pages' => $pages
    ));
  }

  public function addAction(Request $request){

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
    ){
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $em = $this->getDoctrine()->getManager();

    $page  = new Page();
    $page->setAllRoles($this->getRoles());
    $allRolesDump = $this->getRoles();
    $form   = $this->get('form.factory')->create(PageType::class, $page);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $roles = array_values($form->getData()->getAllRoles());
      array_push($roles, "ROLE_ADMINISTRATEUR", "ROLE_RESPONSABLE_LP");
      $page->setRoles($roles);
      $em->persist($page);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', 'Page bien enregistrée.');

      return $this->redirectToRoute('plu_core_licence_homepage');
    }
    return $this->render(
      'PLUCoreBundle:Page:add.html.twig',
      array(
        'form' => $form->createView(),
        'allRolesDump' => $form->getData()
      )
    );
  }

  public function viewAction($id, Request $request){

    $em = $this->getDoctrine()->getManager();
    $page = $em->getRepository('PLUCoreBundle:Page')->findOneById($id);

    $pageRoles = $page->getRoles();
    $isGranted = false;

    if (null == $page) {
      $request->getSession()->getFlashBag()->add('danger', "Cette page n'existe pas.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    foreach ($pageRoles as $key => $roles) {
      if ($this->get('security.authorization_checker')->isGranted($roles)) {
        $isGranted = true;
        break;
      }
    }

    if ($isGranted === false) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    return $this->render('PLUCoreBundle:Page:view.html.twig', array(
      'page' => $page
    ));
  }

  public function deleteAction(Request $request, $id) {

    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
    ) {
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $em = $this->getDoctrine()->getManager();

    $page = $em->getRepository('PLUCoreBundle:Page')->find($id);

    if (null === $page) {
      $request->getSession()->getFlashBag()->add('danger', 'Cette page n\'existe pas');
      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    $form = $this->get('form.factory')->create();

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $em->remove($page);
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', "La page a bien été supprimée.");

      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    return $this->render('PLUCoreBundle:Page:delete.html.twig', array(
      'page' => $page,
      'form'   => $form->createView(),
    ));
  }

  public function editAction($id, Request $request){
    if (
      false === $this->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE_LP')
      && false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRATEUR')
    ){
      $request->getSession()->getFlashBag()->add('danger', "Vous n'avez pas accès à cette page.");
      return $this->redirectToRoute('plu_core_offre_homepage');
    }

    $em = $this->getDoctrine()->getManager();
    $page = $em->getRepository('PLUCoreBundle:Page')->find($id);
    $page->setAllRoles($this->getRoles());

    if (null === $page) {
      $request->getSession()->getFlashBag()->add('danger', "L'offre que vous tentez de modifier n'existe plus.");
      return $this->redirectToRoute('plu_core_offre_homepage');
    }

    $form = $this->get('form.factory')->create(PageType::class, $page);

    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
      $roles = array_values($form->getData()->getAllRoles());
      array_push($roles, "ROLE_ADMINISTRATEUR", "ROLE_RESPONSABLE_LP");
      $page->setRoles($roles);
      // $em->persist($form->getData());
      $em->flush();

      $request->getSession()->getFlashBag()->add('notice', "La page a bien été modifiée.");

      return $this->redirectToRoute('plu_core_licence_homepage');
    }

    return $this->render(
      'PLUCoreBundle:Page:add.html.twig',
      array(
        'page' => $page,
        'form' => $form->createView()
      )
    );
  }

  protected function getRoles() {
    $roles = array();
    $roles["ROLE_VISITEUR"] = "IS_AUTHENTICATED_ANONYMOUSLY";
    foreach ($this->container->getParameter('security.role_hierarchy.roles') as $name => $rolesHierarchy) {
      $roles[$name] = $name;

      foreach ($rolesHierarchy as $role) {
        if (!isset($roles[$role])) {
          $roles[$role] = $role;
        }
      }
    }
    unset($roles["ROLE_RESPONSABLE_LP"]);
    unset($roles["ROLE_ADMINISTRATEUR"]);
    return $roles;
  }

}
