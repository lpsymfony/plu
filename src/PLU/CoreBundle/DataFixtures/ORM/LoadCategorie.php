<?php
// src/OC/PlatformBundle/DataFixtures/ORM/LoadCategory.php

namespace PLU\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PLU\CoreBundle\Entity\Categorie;

class LoadCategorie implements FixtureInterface
{
  // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
  public function load(ObjectManager $manager)
  {
    // Liste des noms de catégorie à ajouter
    $names = array(
      'Stage',
      'CDD',
      'CDI',
      'Alternance contrat d\'apprentissage',
      'Alternance contrat de professionnalisation'
    );

    foreach ($names as $name) {
      // On crée la catégorie
      $categorie = new Categorie();
      $categorie->setNom($name);

      // On la persiste
      $manager->persist($categorie);
    }

    // On déclenche l'enregistrement de toutes les catégories
    $manager->flush();
  }
}
