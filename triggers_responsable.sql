DROP TRIGGER trig_addResponsable;
DROP TRIGGER trig_delResponsable;

DELIMITER |

CREATE TRIGGER trig_addResponsable
AFTER INSERT ON plu_responsable
FOR EACH ROW

BEGIN

  UPDATE plu_utilisateur
  SET roles = 'a:1:{i:0;s:19:\"ROLE_RESPONSABLE_LP\";}'
  WHERE roles <> 'a:1:{i:0;s:19:\"ROLE_RESPONSABLE_LP\";}'
  AND id = NEW.enseignant_id;

END |

CREATE TRIGGER trig_delResponsable
AFTER DELETE ON plu_responsable
FOR EACH ROW

BEGIN

  DECLARE v_responsables integer;

  SELECT COUNT(*) INTO v_responsables
  FROM plu_responsable
  WHERE enseignant_id = OLD.enseignant_id;

  IF v_responsables < 1 THEN
    UPDATE plu_utilisateur
    SET roles = 'a:1:{i:0;s:15:\"ROLE_ENSEIGNANT\";}'
    WHERE id = OLD.enseignant_id;

    UPDATE plu_licence
    SET responsable_id = null
    WHERE responsable_id = OLD.enseignant_id;
  END IF;

END |

DELIMITER ;
