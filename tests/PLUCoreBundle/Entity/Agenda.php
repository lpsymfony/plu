<?php

namespace PLU\CoreBundle\Tests\Entity;

use PLU\CoreBundle\Entity\Agenda;
use PHPUnit\Framework\TestCase;

class AgendaTest extends TestCase{
    private $agenda;

    /**
     * @before
     */
    public function setupSomeFixtures()
    {
        $this->agenda = new Agenda();
    }

    public function testAddLicence()
    {
        $licenceMocked = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);

        $this->agenda->addLicence($licenceMocked);

        $this->assertContains($licenceMocked, $this->agenda->getLicences());
    }

    public function testAddPlusieursLicence()
    {
        $licenceMocked1 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);
        $licenceMocked2 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);
        $licenceMocked3 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);
        $licenceMocked4 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);

        $this->agenda->addLicence($licenceMocked1);
        $this->agenda->addLicence($licenceMocked2);
        $this->agenda->addLicence($licenceMocked3);

        $this->assertContains($licenceMocked1, $this->agenda->getLicences());
        $this->assertContains($licenceMocked2, $this->agenda->getLicences());
        $this->assertContains($licenceMocked3, $this->agenda->getLicences());
        $this->assertNotContains($licenceMocked4, $this->agenda->getLicences());
    }

    public function testRemoveLicence()
    {
        $licenceMocked1 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);
        $licenceMocked2 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);
        $licenceMocked3 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);
        $licenceMocked4 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);

        $this->agenda->addLicence($licenceMocked1);
        $this->agenda->addLicence($licenceMocked2);
        $this->agenda->addLicence($licenceMocked3);

        $this->agenda->removeLicence($licenceMocked2);

        $this->assertContains($licenceMocked1, $this->agenda->getLicences());
        $this->assertNotContains($licenceMocked2, $this->agenda->getLicences());
        $this->assertContains($licenceMocked3, $this->agenda->getLicences());
        $this->assertNotContains($licenceMocked4, $this->agenda->getLicences());
    }
}