<?php

namespace PLU\CoreBundle\Tests\Entity;

use PLU\CoreBundle\Entity\Etudiant;
use PHPUnit\Framework\TestCase;
use PLU\CoreBundle\Entity\Licence;

class EnseignantTest extends TestCase{
    private $etudiant;

    /**
     * @before
     */
    public function setupSomeFixtures()
    {
        $this->etudiant = new Etudiant();
    }

    public function testYearsRange()
    {   
        $yearsRange = $this->etudiant->yearsRange();
        $this->assertEquals($yearsRange[date("Y")], date("Y"));
        $this->assertEquals($yearsRange[date("Y")-49], date("Y")-49);
    }
}