<?php

namespace PLU\CoreBundle\Tests\Entity;

use PLU\CoreBundle\Entity\Licence;
use PLU\CoreBundle\Entity\Etudiant;
use PLU\CoreBundle\Entity\Cours;
use PHPUnit\Framework\TestCase;

class LicenceTest extends TestCase{
    private $licence;

    /**
     * @before
     */
    public function setupSomeFixtures()
    {
        $this->licence = new Licence();
    }

    public function testAddCours()
    {
        $coursMocked = $this->createMock(Cours::class);

        $this->licence->addCour($coursMocked);

        $this->assertContains($coursMocked, $this->licence->getCours());
    }

    public function testAddPlusieursCours()
    {
        $coursMocked1 = $this->createMock(Cours::class);
        $coursMocked2 = $this->createMock(Cours::class);
        $coursMocked3 = $this->createMock(Cours::class);
        $coursMocked4 = $this->createMock(Cours::class);

        $this->licence->addCour($coursMocked1);
        $this->licence->addCour($coursMocked2);
        $this->licence->addCour($coursMocked3);

        $this->assertContains($coursMocked1, $this->licence->getCours());
        $this->assertContains($coursMocked2, $this->licence->getCours());
        $this->assertContains($coursMocked3, $this->licence->getCours());
        $this->assertNotContains($coursMocked4, $this->licence->getCours());
    }

    public function testAddEtudiant()
    {
        $coursMocked = $this->createMock(Etudiant::class);

        $this->licence->addEtudiant($coursMocked);

        $this->assertContains($coursMocked, $this->licence->getEtudiant());
    }

    public function testAddPlusieursEtudiant()
    {
        $etudiantMocked1 = $this->createMock(Etudiant::class);
        $etudiantMocked2 = $this->createMock(Etudiant::class);
        $etudiantMocked3 = $this->createMock(Etudiant::class);
        $etudiantMocked4 = $this->createMock(Etudiant::class);

        $this->licence->addEtudiant($etudiantMocked1);
        $this->licence->addEtudiant($etudiantMocked2);
        $this->licence->addEtudiant($etudiantMocked3);

        $this->assertContains($etudiantMocked1, $this->licence->getEtudiant());
        $this->assertContains($etudiantMocked2, $this->licence->getEtudiant());
        $this->assertContains($etudiantMocked3, $this->licence->getEtudiant());
        $this->assertNotContains($etudiantMocked4, $this->licence->getEtudiant());
    }
}