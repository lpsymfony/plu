<?php

namespace PLU\CoreBundle\Tests\Entity;

use PLU\CoreBundle\Entity\Entreprise;
use PHPUnit\Framework\TestCase;
use PLU\CoreBundle\Entity\Offre;


class EntrepriseTest extends TestCase{
    private $entreprise;

    /**
     * @before
     */
    public function setupSomeFixtures()
    {
        $this->entreprise = new Entreprise();
    }

    public function testaddOffre()
    {
        $offreMocked = $this->createMock(Offre::class);

        $this->entreprise->addOffre($offreMocked);

        $this->assertContains($offreMocked, $this->entreprise->getOffres());
    }

    public function testAddPlusieursOffre()
    {
        $offreMocked1 = $this->createMock(Offre::class);
        $offreMocked2 = $this->createMock(Offre::class);
        $offreMocked3 = $this->createMock(Offre::class);
        $offreMocked4 = $this->createMock(Offre::class);

        $this->entreprise->addOffre($offreMocked1);
        $this->entreprise->addOffre($offreMocked2);
        $this->entreprise->addOffre($offreMocked3);

        $this->assertContains($offreMocked1, $this->entreprise->getOffres());
        $this->assertContains($offreMocked2, $this->entreprise->getOffres());
        $this->assertContains($offreMocked3, $this->entreprise->getOffres());
        $this->assertNotContains($offreMocked4, $this->entreprise->getOffres());
    }

    /*public function testRemoveOffre()
    {
        $offreMocked1 = $this->createMock(Offre::class);
        $offreMocked2 = $this->createMock(Offre::class);
        $offreMocked3 = $this->createMock(Offre::class);
        $offreMocked4 = $this->createMock(Offre::class);

        $this->entreprise->addOffre($offreMocked1);
        $this->entreprise->addOffre($offreMocked2);
        $this->entreprise->addOffre($offreMocked3);

        $this->entreprise->removeOffre($offreMocked2);

        $this->assertContains($offreMocked1, $this->entreprise->getOffres());
        $this->assertNotContains($offreMocked2, $this->entreprise->getOffres());
        $this->assertContains($offreMocked3, $this->entreprise->getOffres());
        $this->assertNotContains($offreMocked4, $this->entreprise->getOffres());
    }*/

}