<?php

namespace PLU\CoreBundle\Tests\Entity;

use PLU\CoreBundle\Entity\Cours;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Filesystem\Filesystem;

class CoursTest extends TestCase{
  private $cours;
  private $file;

  /**
   * @before
   */
  public function setupSomeFixtures()
  {
    $this->cours = new Cours();
    $this->file = new UploadedFile("tests/PLUCoreBundle/Entity/Cours.php", "Cours.php");
  }

  public function testSetFileNull(){
    $this->cours->setFile();

    $this->assertEquals(null, $this->cours->getFile());
  }

  public function testSetFile(){
    $this->cours->setFile($this->file);

    $this->assertEquals($this->file, $this->cours->getFile());
  }

  public function testPreUploadEmpty(){
    $this->assertEquals(null, $this->cours->preUpload());
  }

  public function testPreUpload(){
    $this->cours->setFile($this->file);
    $this->cours->preUpload();
    $this->assertEquals("Cours.php", $this->cours->getFichiers());
  }

  public function testPreRemoveUpload(){
    $this->cours->setOldId("Cours");
    $this->cours->setOldPath("tests/PLUCoreBundle/Entity");
    $this->cours->setOldFichiers("ACPI.php");
    $this->cours->preRemoveUpload();

    $this->assertEquals("cours/tests/PLUCoreBundle/Entity/Cours_ACPI.php", $this->cours->getTempFilename());
  }
}