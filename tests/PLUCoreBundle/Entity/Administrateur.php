<?php

namespace PLU\CoreBundle\Tests\Entity;

use PLU\CoreBundle\Entity\Administrateur;
use PHPUnit\Framework\TestCase;

class AdministrateurTest extends TestCase{
    public function testConstructeur(){
        $admin = new Administrateur();
        $this->assertEquals(array('ROLE_ADMINISTRATEUR'), $admin->getRoles());
    }
}