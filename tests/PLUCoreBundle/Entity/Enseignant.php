<?php

namespace PLU\CoreBundle\Tests\Entity;

use PLU\CoreBundle\Entity\Enseignant;
use PHPUnit\Framework\TestCase;

class EnseignantTest extends TestCase{
    private $enseignant;

    /**
     * @before
     */
    public function setupSomeFixtures()
    {
        $this->enseignant = new Enseignant();
    }

    public function testAddLicence()
    {
        $licenceMocked = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);

        $this->enseignant->addLicences($licenceMocked);

        $this->assertContains($licenceMocked, $this->enseignant->getLicences());
    }

    public function testAddPlusieursLicence()
    {
        $licenceMocked1 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);
        $licenceMocked2 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);
        $licenceMocked3 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);
        $licenceMocked4 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);

        $this->enseignant->addLicences($licenceMocked1);
        $this->enseignant->addLicences($licenceMocked2);
        $this->enseignant->addLicences($licenceMocked3);

        $this->assertContains($licenceMocked1, $this->enseignant->getLicences());
        $this->assertContains($licenceMocked2, $this->enseignant->getLicences());
        $this->assertContains($licenceMocked3, $this->enseignant->getLicences());
        $this->assertNotContains($licenceMocked4, $this->enseignant->getLicences());
    }

    public function testRemoveLicence()
    {
        $licenceMocked1 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);
        $licenceMocked2 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);
        $licenceMocked3 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);
        $licenceMocked4 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);

        $this->enseignant->addLicences($licenceMocked1);
        $this->enseignant->addLicences($licenceMocked2);
        $this->enseignant->addLicences($licenceMocked3);

        $this->enseignant->removeLicence($licenceMocked2);

        $this->assertContains($licenceMocked1, $this->enseignant->getLicences());
        $this->assertNotContains($licenceMocked2, $this->enseignant->getLicences());
        $this->assertContains($licenceMocked3, $this->enseignant->getLicences());
        $this->assertNotContains($licenceMocked4, $this->enseignant->getLicences());
    }

    public function testLicenceArray()
    {
        $licenceMocked1 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);
        $licenceMocked2 = $this->createMock(\PLU\CoreBundle\Entity\Licence::class);
        $licenceMocked1->expects($this->any())
                    ->method('getId')
                    ->willReturn(1);
        $licenceMocked2->expects($this->any())
                    ->method('getId')
                    ->willReturn(2);

        $this->enseignant->addLicences($licenceMocked1);
        $this->enseignant->addLicences($licenceMocked2);

        $licenceArray =  $this->enseignant->getLicencesArray();

        $this->assertContains($licenceMocked1->getId(), $licenceArray);
        $this->assertContains($licenceMocked2->getId(), $licenceArray);
    }
}