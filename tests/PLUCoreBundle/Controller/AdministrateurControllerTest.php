<?php

namespace PLU\CoreBundle\Tests\Controller;

use PLU\CoreBundle\Entity\Administrateur;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdministrateurControllerTest extends WebTestCase
{
    public function testIndexPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/administrateur');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testAddPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/administrateur/add');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testDeletePasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/administrateur/delete/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testEditPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/administrateur/edit/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    /*public function testIndexOk(){

        $client = static::createClient();

        $administrateur = new Administrateur();
        /*$administrateur->setSalt($admin->getSalt());
        $encoded = $client->getContainer()->get('security.password_encoder')->encodePassword($administrateur, 'test');
        $administrateur->setPassword("admin");
        $administrateur->setUsername("admin");
        $administrateur->setNom("nom");
        $administrateur->setPrenom("prenom");
        $administrateur->setId(59);
        var_dump($administrateur);

        $adminRepository = $this->createMock(Administrateur::class);
        $adminRepository->expects($this->any())
                    ->method('getId')
                    ->willReturn($admin->getId());
        $adminRepository->expects($this->any())
                    ->method('getPassword')
                    ->willReturn($admin->getPassword());
        $adminRepository->expects($this->any())
                    ->method('getRoles')
                    ->willReturn($admin->getRoles());
        $adminRepository->expects($this->any())
                    ->method('getUsername')
                    ->willReturn($admin->getUsername());

        $firewallContext = "main";

        $token = new UsernamePasswordToken($adminRepository, null, $firewallContext, array('ROLE_ADMINISTRATEUR'));
        $session = static::$kernel->getContainer()->get('session');

        $session->set('_security_main', serialize($token));
        $session->save();
        $client->getCookieJar()->set(new Cookie($session->getName(), $session->getId()));

        $client->expects($this->any())
                    ->method('find')
                    ->willReturn($admin);

        $crawler = $client->request('GET', '/administrateur');
        $crawler = $client->request('GET', '/administrateur');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

    }*/
}
