<?php

namespace PLU\CoreBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LicenceControllerTest extends WebTestCase
{
    public function testIndexAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertContains("Liste des Licences", $client->getResponse()->getContent());
    }

    public function testAddPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/licence/add');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testDeletePasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/licence/delete/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testEditPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/licence/edit/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }
}