<?php

namespace PLU\CoreBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class PageControllerTest extends WebTestCase
{
    public function testAddPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/page/add');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testDeletePasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/page/delete/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testEditPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/page/edit/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }
}