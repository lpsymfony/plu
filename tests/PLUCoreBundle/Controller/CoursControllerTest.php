<?php

namespace PLU\CoreBundle\Tests\Controller;

use PLU\CoreBundle\Entity\Agenda;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CoursControllerTest extends WebTestCase
{
    public function testIndexPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/cours');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testAddPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/cours/add');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testDeletePasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/cours/delete/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testEditPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/cours/edit/1/lol/lol');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testCreateFolderPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/cours/createFolder');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testDeleteFolderPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/cours/deleteFolder/lol');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testEditFolderPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/cours/editFolder/lol');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testDownloadPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/cours/download/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testViewPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/cours/view/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }
}