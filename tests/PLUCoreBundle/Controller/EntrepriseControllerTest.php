<?php

namespace PLU\CoreBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EntrepriseControllerTest extends WebTestCase
{
    public function testIndexPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/entreprise');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testAddPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/entreprise/add');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testDeletePasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/entreprise/delete/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testEditPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/entreprise/edit/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }
}