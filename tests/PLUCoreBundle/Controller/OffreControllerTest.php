<?php

namespace PLU\CoreBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class OffreControllerTest extends WebTestCase
{
    public function testIndexPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/offre/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testViewPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/offre/view/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testDownloadPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/offre/download/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testAddPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/offre/add');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testDeletePasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/offre/delete/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testEditPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/offre/edit/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testChangeVisiblePasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/offre/changeaccept/1');

        $this->assertContains("error", $client->getResponse()->getContent());
    }

    public function testChangeAcceptPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/offre/changevisible/1');

        $this->assertContains("error", $client->getResponse()->getContent());
    }
}