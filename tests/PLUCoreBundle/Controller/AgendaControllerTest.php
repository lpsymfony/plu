<?php

namespace PLU\CoreBundle\Tests\Controller;

use PLU\CoreBundle\Entity\Agenda;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AgendaControllerTest extends WebTestCase
{
    public function testIndexPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/agenda/index');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testAddPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/agenda/add');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testDeletePasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/agenda/delete/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testEditPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/agenda/edit/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }
}