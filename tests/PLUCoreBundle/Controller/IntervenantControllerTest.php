<?php

namespace PLU\CoreBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class IntervenantControllerTest extends WebTestCase
{
    public function testIndexAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/intervenant');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testAddPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/intervenant/add');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testDeletePasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/intervenant/delete/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testEditPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/intervenant/edit/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }
}