<?php

namespace PLU\CoreBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

class ForumControllerTest extends WebTestCase
{
    public function testIndexPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/foorum');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testAddPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/forum/add');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }
/*
    public function testDeletePasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/forum/delete/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testEditPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/forum/edit/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }

    public function testViewPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/forum/view/1');

        $this->assertContains("Redirecting to", $client->getResponse()->getContent());
    }*/

    public function testResoluPasDeDroitDAccesALaPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/forum/resolu/1');

        $response = new JsonResponse();
        $response->setData(
            array('error' => 'Vous n\'avez pas accès à cette fonction')
        );
        $this->assertContains('error', $client->getResponse()->getContent());
    }
}